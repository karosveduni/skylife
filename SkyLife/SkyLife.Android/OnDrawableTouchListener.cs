﻿using Android.Text.Method;
using Android.Views;
using Android.Widget;

using Xamarin.Forms;

[assembly: ExportEffect(typeof(SkyLife.Droid.ShowHidePassEffect), "ShowHidePassEffect")]

namespace SkyLife.Droid
{
    /// <summary>
    /// Класс в котором идет реализация эффекта.
    /// </summary>
    public class OnDrawableTouchListener : Java.Lang.Object, Android.Views.View.IOnTouchListener
    {
        /// <summary>
        /// Вызывается при сработает эффекта.
        /// </summary>
        /// <param name="v">view.</param>
        /// <param name="e">args.</param>
        /// <returns>Возвращает знчение прошло ли все хорошо.</returns>
        public bool OnTouch(Android.Views.View v, MotionEvent e)
        {
            if (v is EditText text && e.Action is MotionEventActions.Up)
            {
                EditText editText = text;
                if (e.RawX >= (editText.Right - editText.GetCompoundDrawables()[2].Bounds.Width()))
                {
                    if (editText.TransformationMethod is null)
                    {
                        editText.TransformationMethod = PasswordTransformationMethod.Instance;
                        editText.SetCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, Resource.Drawable.ShowPass, 0);
                    }
                    else
                    {
                        editText.TransformationMethod = null;
                        editText.SetCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, Resource.Drawable.HidePass, 0);
                    }

                    return true;
                }
            }

            return false;
        }
    }
}