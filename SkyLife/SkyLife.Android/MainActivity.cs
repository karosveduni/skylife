﻿namespace SkyLife.Droid
{
	using Android.App;
	using Android.Content.PM;
	using Android.OS;
	using Android.Runtime;

	using Sharpnado.CollectionView.Droid;

	[Activity(Label = "SkyLife", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize)]
	public partial class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		public static Activity Current;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			Current = this;

			base.OnCreate(savedInstanceState);
			NativeMedia.Platform.Init(this, savedInstanceState);

			FFImageLoading.Forms.Platform.CachedImageRenderer.Init(true);

			Initializer.Initialize();

			Xamarin.Essentials.Platform.Init(this, savedInstanceState);
			global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

			FFImageLoading.Forms.Platform.CachedImageRenderer.InitImageViewHandler();
			LoadApplication(new App(new AndroidInitializer()));
		}

		public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
		{
			Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

			base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}
}