﻿using System.Threading.Tasks;

using Android.OS;

using AndroidX.Core.View;

using SkyLife.Droid.CustomRenderers;
using SkyLife.Interfaces;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: Dependency(typeof(StatusBarAndroid))]

namespace SkyLife.Droid.CustomRenderers
{
	public class StatusBarAndroid : IStatusBarPlatformSpecific
	{
		public async void SetStatusBarColor(Color color, bool darkStatusBarTint = false)
		{
			if (Build.VERSION.SdkInt < Android.OS.BuildVersionCodes.Lollipop)
				return;

			MainActivity.Current.Window.AddFlags(Android.Views.WindowManagerFlags.DrawsSystemBarBackgrounds);
			MainActivity.Current.Window.ClearFlags(Android.Views.WindowManagerFlags.TranslucentStatus);

			if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.M)
			{
				await Task.Delay(50);
				WindowCompat.GetInsetsController(MainActivity.Current.Window, MainActivity.Current.Window.DecorView).AppearanceLightStatusBars = darkStatusBarTint;
			}

			MainActivity.Current.Window.SetStatusBarColor(color.ToAndroid());
		}
	}
}