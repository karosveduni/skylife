﻿using SkyLife.Droid;
using SkyLife.Interfaces;

using Xamarin.Forms;

[assembly: Dependency(typeof(CloseApplication))]

namespace SkyLife.Droid
{
    public class CloseApplication : ICloseApplication
    {
        void ICloseApplication.CloseApplication()
        {
            MainActivity.Current.FinishAndRemoveTask();
            //Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        }
    }
}