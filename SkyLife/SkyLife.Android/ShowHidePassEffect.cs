﻿using Android.Widget;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ResolutionGroupName("Xamarin")]
[assembly: ExportEffect(typeof(SkyLife.Droid.ShowHidePassEffect), "ShowHidePassEffect")]

namespace SkyLife.Droid
{
    /// <summary>
    /// Реализация эффекта для андроида.
    /// </summary>
    public class ShowHidePassEffect : PlatformEffect
    {
        /// <summary>
        /// Метод в котором идет присоединение эффекта.
        /// </summary>
        protected override void OnAttached()
        {
            ConfigureControl();
        }

        /// <summary>
        /// Метод в котором идет отсоединение эффекта.
        /// </summary>
        protected override void OnDetached()
        {
        }

        private void ConfigureControl()
        {
            EditText editText = (EditText)Control;
            editText.SetCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, Resource.Drawable.ShowPass, 0);
            editText.SetOnTouchListener(new OnDrawableTouchListener());
        }

    }
}