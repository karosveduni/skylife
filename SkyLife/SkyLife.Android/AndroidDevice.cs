﻿using Android.Provider;
using SkyLife.Droid;
using SkyLife.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(AndroidDevice))]
namespace SkyLife.Droid
{
    public class AndroidDevice : IDevice
    {
        [System.Obsolete]
        public string GetIdentifier()
        {
            return Settings.Secure.GetString(Forms.Context.ContentResolver, Settings.Secure.AndroidId);
        }
    }
}