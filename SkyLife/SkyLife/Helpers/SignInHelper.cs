﻿namespace SkyLife.Helpers
{
    using Database.Enum;
    using SkyLife.Resources.ConstantValues;

	public static class SignInHelper
	{
		public static string GetStringPage(UserRole role) => role switch
		{
			UserRole.Admin => string.Empty,
			UserRole.Client => ConstantString.ClientPage,
			UserRole.Manager => ConstantString.ManagerPage,
			UserRole.Trainer => ConstantString.TrainerPage,
			_ => string.Empty,
		};
	}
}
