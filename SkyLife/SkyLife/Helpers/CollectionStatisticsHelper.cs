﻿namespace SkyLife.Helpers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Prism.DryIoc;

    using SkyLife.Model;

    using Xamarin.Essentials;

    public class CollectionStatisticsHelper
    {
        public void Statistics()
        {
            if (Preferences.Get("collect", false))
			{
				return;
			}

			DeviceInfoHelper device = new DeviceInfoHelper();

            (PrismApplication.Current.Container.Resolve(typeof(Storage)) as Storage).Statistics.Add(device);

            Preferences.Set("collect", true);
        }

        public Task<IEnumerable<DeviceInfoHelper>> GetStatistics()
        {
            return Task.FromResult(Enumerable.Empty<DeviceInfoHelper>());
        }

    }
}
