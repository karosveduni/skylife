﻿namespace SkyLife.Helpers
{
    using System.Text.RegularExpressions;

    public class PasswordHelper
    {
        public string Pattern { get; } = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$";

        public bool IsCheckFormat(string password) => Regex.IsMatch(password, Pattern);
    }
}
