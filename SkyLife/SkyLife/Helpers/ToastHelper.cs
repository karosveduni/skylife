﻿namespace SkyLife.Helpers
{
    using System;
    using System.Threading.Tasks;

    using Xamarin.CommunityToolkit.Extensions;
    using Xamarin.CommunityToolkit.UI.Views.Options;
    using Xamarin.Forms;

    /// <summary>
    /// Класс показывающий Toast, SnackBar.
    /// </summary>
    public class ToastHelper
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToastHelper"/> class.
        /// </summary>
        public ToastHelper()
        {
            toastOptions = new ToastOptions
            {
                BackgroundColor = (Color)Application.Current.Resources["BackgroundColorButton"],
                CornerRadius = new Thickness(0),
                Duration = TimeSpan.FromSeconds(3),
                MessageOptions = new MessageOptions
                {
                    Foreground = (Color)Application.Current.Resources["ForegroundColorTextButton"],
                    Padding = new Thickness(0),
                },
            };
            snackBarOptions = new SnackBarOptions
            {
                BackgroundColor = (Color)Application.Current.Resources["BackgroundColorButton"],
                CornerRadius = new Thickness(0),
                Duration = TimeSpan.FromSeconds(3),
                MessageOptions = new MessageOptions
                {
                    Foreground = (Color)Application.Current.Resources["ForegroundColorTextButton"],
                    Padding = new Thickness(0),
                },
            };
            snackBarActionOptions = new SnackBarActionOptions
            {
                BackgroundColor = (Color)Application.Current.Resources["BackgroundColorButton"],
                ForegroundColor = (Color)Application.Current.Resources["ForegroundColorTextButton"],
                Padding = new Thickness(0),
            };
        }

        private readonly ToastOptions toastOptions;
        private readonly SnackBarOptions snackBarOptions;
        private readonly SnackBarActionOptions snackBarActionOptions;

        /// <summary>
        /// Класс показывающий Toast.
        /// </summary>
        /// <param name="message">Строка для показа на экране.</param>
        /// <returns>Возвращает таску.</returns>
        public Task ShowToastAsync(string message) => ShowToastAsync(Application.Current.MainPage, message);

        /// <summary>
        /// Показывает на элементе.
        /// </summary>
        /// <param name="element">Элемент к которому будет привязанна всплывающий текст.</param>
        /// <param name="message">Строка для показа.</param>
        /// <returns>Возвращает таску.</returns>
        public Task ShowToastAsync(VisualElement element, string message)
        {
            toastOptions.MessageOptions.Message = message;
            return element.DisplayToastAsync(toastOptions);
        }

        /// <summary>
        /// Показывает SnackBar.
        /// </summary>
        /// <param name="message">Строка для показа.</param>
        /// <param name="text">Текст на кнопку.</param>
        /// <param name="func">Функция выполнения при нажатии на кнопку.</param>
        /// <returns>Возвращает таску.</returns>
        public Task ShowSnackBarAsync(string message, string text, Func<Task> func) => ShowSnackBarAsync(Application.Current.MainPage, message, text, func);

        /// <summary>
        /// Показывает SnackBar.
        /// </summary>
        /// <param name="element">Элемент к которому приязан SnackBar.</param>
        /// <param name="message">Строка для показа.</param>
        /// <param name="text">Текст на кнопку.</param>
        /// <param name="func">Функция выполнения при нажатии на кнопку.</param>
        /// <returns>Возвращает таску.</returns>
        public Task ShowSnackBarAsync(VisualElement element, string message, string text, Func<Task> func)
        {
            snackBarOptions.MessageOptions.Message = message;
            snackBarActionOptions.Text = text;
            snackBarActionOptions.Action = func;
            snackBarOptions.Actions = new SnackBarActionOptions[] { snackBarActionOptions };
            return element.DisplaySnackBarAsync(snackBarOptions);
        }
    }
}
