﻿namespace SkyLife.Helpers
{
    using System.Threading.Tasks;

    using Xamarin.Essentials;

    public class PermissionsHepler
    {
        public static async Task<bool> CheckingForPermissionsAsync<T>(PermissionStatus status = PermissionStatus.Granted)
            where T : Permissions.BasePermission, new()
        {
            PermissionStatus result = await Permissions.CheckStatusAsync<T>();

            return result.Equals(status) ? true : Permissions.RequestAsync<T>().GetAwaiter().GetResult().Equals(result);


			/*if (!result.Equals(status))
            {
                //result = await Permissions.RequestAsync<T>();
                //if (!result.Equals(status))
                //{
                //    return false;
                //}

                return Permissions.RequestAsync<T>().GetAwaiter().GetResult().Equals(result);
            }*/

            //return true;
        }
    }
}
