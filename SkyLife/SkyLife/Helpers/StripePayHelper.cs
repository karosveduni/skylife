﻿namespace SkyLife.Helpers
{
    public class StripePayHelper
    {
        public StripePayHelper()
        {
            Price = 0;
            Description = string.Empty;
        }

        public float Price { get; set; }

        public string Description { get; set; }
    }
}
