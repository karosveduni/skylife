﻿namespace SkyLife.Helpers
{
    using System.Globalization;
    using System.Reflection;

    using SkyLife.Attribute;
    using SkyLife.Enum;

    public class EnumLanguageHelper
    {
        public CultureInfoLanguageAttribute GetCultureInfoFromEnum(Language language)
        {
            FieldInfo fieldInfo = language.GetType().GetField(language.ToString());
            object[] attributes = fieldInfo.GetCustomAttributes(typeof(CultureInfoLanguageAttribute), true);
            return (CultureInfoLanguageAttribute)attributes[0];
        }

        public CultureInfoLanguageAttribute GetCultureInfoFromEnum(int language) => GetCultureInfoFromEnum((Language)language);

        public Language GetLanguageFromCultureInfo(CultureInfo culture)
        {
            return culture.Name switch
            {
                "ru-RU" => Language.Russian,
                "uk-UA" => Language.Ukrainian,
                _ => Language.English,
            };
        }
    }
}
