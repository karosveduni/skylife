﻿namespace Database.Interfaces
{
    using System;

    using Database.Enum;

    /// <summary>
    /// Базовый интерфейс для всех пользователей.
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// Gets роль пользователя.
        /// </summary>
        UserRole Role { get; }

        /// <summary>
        /// Gets iD пользователя.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Gets ссылка на фото.
        /// </summary>
        byte[] Image { get; }

        /// <summary>
        /// Gets имя пользователя.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets поле для хранения почты.
        /// </summary>
        string Email { get; }

        /// <summary>
        /// Gets поле для хранения пароля.
        /// </summary>
        string Password { get; }

        /// <summary>
        /// Gets a value indicating whether поле для хранения значения верифицировал ли пользователь сво почту.
        /// </summary>
        bool IsEmailVerification { get; }

        /// <summary>
        /// Хранит значение для QRCode.
        /// </summary>
        string QRCode { get; }

        /// <summary>
        /// Очищает картинку.
        /// </summary>
        public void ClearImage();
    }
}
