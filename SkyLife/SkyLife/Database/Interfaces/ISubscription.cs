﻿using Database.Enum;
using System;
using System.Collections.Generic;

namespace Database.Interfaces
{
    public interface ISubscription
    {
        NumberWorkouts NumberWorkouts { get; }
        DateTime Start { get; }
        DateTime End { get; }
        List<DateTime?> Visits { get; }
        bool IsPayment { get; }
        string IdStripePaymentMethod { get; }
    }
}
