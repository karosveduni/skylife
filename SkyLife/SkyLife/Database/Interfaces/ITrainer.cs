﻿namespace Database.Interfaces
{
    using System.Collections.Generic;

    using Database.Model;

    public interface ITrainer
    {
        float Grade { get; }
        List<Comment> Comments { get; }
    }
}
