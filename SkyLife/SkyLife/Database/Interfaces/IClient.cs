﻿using Database.Model;
using System.Collections.Generic;

namespace Database.Interfaces
{
    public interface IClient
    {
        Subscription Subscription { get; }
        Trainer Trainer { get; }
        List<string> StripePaymentMethod { get; }
    }
}
