﻿namespace Database.Interfaces
{
    using System;

    /// <summary>
    /// Хранит информацию про комментарий оставленный про тренера.
    /// </summary>
    public interface IComment
    {
        /// <summary>
        /// Gets хранит Id тренера.
        /// </summary>
        Guid TrainerId { get; }

        /// <summary>
        /// Gets or sets строка комментария.
        /// </summary>
        string Message { get; set; }

        /// <summary>
        /// Gets информацию про оценку.
        /// </summary>
        byte Grade { get; }

        /// <summary>
        /// Gets хранит информацию про Id клиента, который оставил комментарий.
        /// </summary>
        Guid ClientId { get; }
    }
}
