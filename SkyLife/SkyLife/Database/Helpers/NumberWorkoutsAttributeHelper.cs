﻿namespace Database.Helpers
{
    using System;
    using System.Reflection;

    using Database.Attribute;
    using Database.Enum;
    using Database.Exception;

    /// <summary>
    /// Класс помощник для работы со списком NumberWorkouts.
    /// </summary>
    public class NumberWorkoutsAttributeHelper
    {
        private static NumberWorkoutsAttributeHelper current;

        public static NumberWorkoutsAttributeHelper Current => current ??= new NumberWorkoutsAttributeHelper();

        private readonly NumberWorkouts[] array = new NumberWorkouts[]
        {
            NumberWorkouts.OneTimeWorkout, NumberWorkouts.OneTimeWorkoutWithTrainer,
            NumberWorkouts.FourWorkouts, NumberWorkouts.FourWorkoutsWithTrainer,
            NumberWorkouts.SixWorkouts, NumberWorkouts.SixWorkoutsWithTrainer,
            NumberWorkouts.EightWorkouts, NumberWorkouts.EightWorkoutsWithTrainer,
            NumberWorkouts.TwelveWorkouts, NumberWorkouts.TwelveWorkoutsWithTrainer,
            NumberWorkouts.NoLimits, NumberWorkouts.NoLimitsWithTrainer,
        };

        /// <summary>
        /// Еовертирует NumberWorkouts в класс InformationAboutWorkoutAttribute.
        /// </summary>
        /// <param name="numberWorkouts">Значение которое надо конвертнуть.</param>
        /// <returns>Возвращает объект класса InformationAboutWorkoutAttribute.</returns>
        public InformationAboutWorkoutAttribute ConvertItemToAttribute(NumberWorkouts numberWorkouts)
        {
            FieldInfo fieldInfo = numberWorkouts.GetType().GetField(numberWorkouts.ToString());
            object[] attributes = fieldInfo.GetCustomAttributes(typeof(InformationAboutWorkoutAttribute), true);
            return (InformationAboutWorkoutAttribute)attributes[0];
        }

        /// <summary>
        /// Конвертирует атрибут в NumberWorkouts.
        /// </summary>
        /// <param name="attribute">Объект атрибута.</param>
        /// <returns>Возвращает запись из списка NumberWorkouts.</returns>
        /// <exception cref="NoMatchingValuesException">КИнет исключение если не найдет совпадения.</exception>
        public NumberWorkouts ConvertAttributeToItem(InformationAboutWorkoutAttribute attribute)
            => BaseConverter(info => info.Equals(attribute));

        /// <summary>
        /// Конвертирует строку в список NumberWorkouts.
        /// </summary>
        /// <param name="attributeInfo">Строка для конвертации.</param>
        /// <returns>Возвращает список NumberWorkouts.</returns>
        /// <exception cref="NoMatchingValuesException">Кинет исключение если не найдет совпадений.</exception>
        public NumberWorkouts ConvertAttributeToItem(string attributeInfo)
            => BaseConverter(info => info.Info.Equals(attributeInfo));

        private NumberWorkouts BaseConverter(Func<InformationAboutWorkoutAttribute, bool> func)
        {
            int index = 0;
            while (index < array.Length)
            {
                InformationAboutWorkoutAttribute value = ConvertItemToAttribute(array[index]);
                if (func(value))
                {
                    return array[index];
                }
            }

            throw new NoMatchingValuesException();
        }
    }
}
