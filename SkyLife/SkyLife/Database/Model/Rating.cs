﻿namespace Database.Model
{
	public class Rating
	{
		public byte Grade { get; set; }

		public string Comment { get; set; }
	}
}
