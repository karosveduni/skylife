﻿namespace Database.Model
{
	using System;

	using Database.Enum;
	using Database.Interfaces;

	using Xamarin.CommunityToolkit.ObjectModel;

	/// <summary>
	/// Базовый класс для всех пользователей.
	/// </summary>
	public abstract class User : ObservableObject, IUser
	{
		/// <inheritdoc/>
		public UserRole Role { get; protected set; }

		/// <inheritdoc/>
		public Guid Id { get; private set; }

		/// <inheritdoc/>
		public byte[] Image { get; set; }

		/// <inheritdoc/>
		public string Name { get; set; }

		/// <inheritdoc/>
		public string Email { get; set; }

		/// <inheritdoc/>
		public string Password { get; set; }

		/// <inheritdoc/>
		public bool IsEmailVerification { get; set; }

		/// <inheritdoc/>
		public string QRCode { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="User"/> class.
		/// </summary>
		protected User()
		{
			Id = Guid.NewGuid();
		}

		public virtual void ClearImage()
		{
			Image = null;
		}
	}
}
