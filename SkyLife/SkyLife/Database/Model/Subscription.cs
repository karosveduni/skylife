﻿using System;
using System.Collections.Generic;

using Database.Enum;
using Database.Interfaces;

namespace Database.Model
{
    /// <summary>
    /// Хранит информацию про абонемент.
    /// </summary>
    public class Subscription : ISubscription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Subscription"/> class.
        /// </summary>
        /// <param name="numberWorkouts">Количество тренировок.</param>
        /// <param name="start">Начало абонемента.</param>
        /// <param name="end">Конец абонемента.</param>
        /// <param name="isPayment">Оплачен ли абонемент.</param>
        /// <param name="idStripePaymentMethod">Строка id про оплату.</param>
        public Subscription(NumberWorkouts numberWorkouts, DateTime start, DateTime end, bool isPayment, string idStripePaymentMethod)
        {
            NumberWorkouts = numberWorkouts;
            Start = start;
            End = end;
            IsPayment = isPayment;
            IdStripePaymentMethod = idStripePaymentMethod;
            var attribute = Helpers.NumberWorkoutsAttributeHelper.Current.ConvertItemToAttribute(numberWorkouts);
            if (attribute.CountWorkout != -1)
            {
                Visits = new List<DateTime?>(new DateTime?[attribute.CountWorkout]);
            }
            else
            {
                Visits = new List<DateTime?>(new DateTime?[((int)(end - start).TotalDays)]);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Subscription"/> class.
        /// </summary>
        /// <param name="numberWorkouts">Количество тренировок.</param>
        /// <param name="start">Начало абонемента.</param>
        /// <param name="end">Конец абонемента.</param>
        public Subscription(NumberWorkouts numberWorkouts, DateTime start, DateTime end)
            : this(numberWorkouts, start, end, false, string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Subscription"/> class.
        /// </summary>
        public Subscription()
            : this(default, DateTime.MinValue, DateTime.MinValue, false, string.Empty)
        {
            Visits = new List<DateTime?>();
        }

        /// <summary>
        /// Gets хранит информацию про количество тренировок.
        /// </summary>
        public NumberWorkouts NumberWorkouts { get; private set; }

        /// <summary>
        /// Gets начало действия абонемента.
        /// </summary>
        public DateTime Start { get; private set; }

        /// <summary>
        /// Gets конец действия абонемента.
        /// </summary>
        public DateTime End { get; private set; }

        /// <summary>
        /// Gets хранит информацию про визит клиента в спорт зал.
        /// </summary>
        public List<DateTime?> Visits { get; private set; }

        /// <summary>
        /// Gets a value indicating whether. Хранит информацию про то оплачен ли текущий абонемент.
        /// </summary>
        public bool IsPayment { get; private set; }

        /// <summary>
        /// Gets хранит Id последней оплаты.
        /// </summary>
        public string IdStripePaymentMethod { get; private set; }
    }
}
