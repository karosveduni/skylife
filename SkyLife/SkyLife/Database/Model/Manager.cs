﻿using Database.Enum;
using Database.Interfaces;

namespace Database.Model
{
    /// <summary>
    /// Класс для хранения информации про менеджера.
    /// </summary>
    public class Manager : User, IManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Manager"/> class.
        /// </summary>
        public Manager()
        {
            Role = UserRole.Manager;
        }
    }
}
