﻿namespace Database.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Database.Enum;
    using Database.Interfaces;

    using Prism.DryIoc;

    using SkyLife.Model;

    /// <summary>
    /// Хранит информацию про тренера.
    /// </summary>
    public partial class Trainer : User, ITrainer
    {
        private Storage storage;

        /// <summary>
        /// Initializes a new instance of the <see cref="Trainer"/> class.
        /// </summary>
        public Trainer()
            : this(Array.Empty<byte>(), string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Trainer"/> class.
        /// </summary>
        /// <param name="photoUrl">Ссылка на фото.</param>
        /// <param name="name">Имя тренера.</param>
        public Trainer(byte[] photoUrl, string name)
        {
            Role = UserRole.Trainer;
            Image = photoUrl;
            Name = name;

            storage = PrismApplication.Current.Container.Resolve(typeof(Storage)) as Storage;

            Initialization();
        }

        /// <summary>
        /// Gets or Sets ранит список коментариев оставленных для этого тренера.
        /// </summary>
        public List<Comment> Comments { get; private set; }

        /// <summary>
        /// Gets хранит информацию про средний бал.
        /// </summary>
        public float Grade { get; private set; }

        private void Initialization()
        {
            Comments = storage.Comments.FindAll(item => item.TrainerId == Id);
            Grade = Comments.Count is 0 ? 0 : Comments.Average(item => (float)item.Grade);
        }
    }
}
