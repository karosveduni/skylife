﻿namespace Database.Model
{
    using System;

    using Database.Interfaces;

    /// <summary>
    /// Хранит информацию про комментарий оставленный про тренера.
    /// </summary>
    public class Comment : IComment
    {
        /// <summary>
        /// Gets or sets хранит Id тренера.
        /// </summary>
        public Guid TrainerId { get; set; }

        /// <summary>
        /// Gets or sets хранит информацию про оценку.
        /// </summary>
        public byte Grade { get; set; }

        /// <summary>
        /// Gets or sets хранит информацию про Id клиента, который оставил комментарий.
        /// </summary>
        public Guid ClientId { get; set; }

        /// <summary>
        /// Gets or sets строка комментария.
        /// </summary>
        public string Message { get; set; }
    }
}
