﻿namespace Database.Model
{
    using System.Collections.Generic;

    using Database.Enum;
    using Database.Interfaces;

    /// <summary>
    /// Класс для хранения информации про клиентов.
    /// </summary>
    public class Client : User, IClient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Client"/> class.
        /// </summary>
        public Client()
            : this(new Subscription(), new List<string>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Client"/> class.
        /// </summary>
        /// <param name="subscription">Информация про абонемент клиента.</param>
        public Client(Subscription subscription) 
            : this(subscription, new List<string>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Client"/> class.
        /// </summary>
        /// <param name="subscription">Информация про абонемент клиента.</param>
        /// <param name="stripePaymentMethod">Информация про оплату всех абонементов.</param>
        public Client(Subscription subscription, List<string> stripePaymentMethod)
        {
            Role = UserRole.Client;
            Subscription = subscription;
            StripePaymentMethod = stripePaymentMethod;
        }

        /// <summary>
        /// Gets свойство для хранения информации про абонемент.
        /// </summary>
        public Subscription Subscription { get; private set; }

        /// <summary>
        /// Gets or sets хранит информацию про тренера который закреплен за клиентом.
        /// </summary>
        public Trainer Trainer { get; set; }

        /// <summary>
        /// Gets хранит информацию про оплату все абонементов.
        /// </summary>
        public List<string> StripePaymentMethod { get; private set; }
    }
}
