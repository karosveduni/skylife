﻿using Database.Enum;
using Database.Interfaces;

namespace Database.Model
{
    /// <summary>
    /// Класс отвечающий за хранения инфорамции про Admin
    /// </summary>
    public class Admin : User, IAdmin
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Admin"/> class.
        /// </summary>
        public Admin()
        {
            Role = UserRole.Admin;
        }
    }
}
