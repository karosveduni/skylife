﻿namespace Database.Enum
{
    /// <summary>
    /// Список ролей пользователя.
    /// </summary>
    public enum UserRole : byte
    {
        /// <summary>
        /// Первый уровень "Клиент".
        /// </summary>
        Client = 0,

        /// <summary>
        /// Второй уровень "Тренер".
        /// </summary>
        Trainer = 1,

        /// <summary>
        /// Третий уровень "Менеджер".
        /// </summary>
        Manager = 2,

        /// <summary>
        /// Четвртый уровень "Админ".
        /// </summary>
        Admin = 3,
    }
}
