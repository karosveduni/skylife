﻿namespace Database.Enum
{
    using Database.Attribute;

    /// <summary>
    /// Хранит список количество тренировок.
    /// </summary>
    public enum NumberWorkouts : byte
    {
        /// <summary>
        /// Одна тренировка.
        /// </summary>
        [InformationAboutWorkout("Одна тренировка", 70f, 1)]
        OneTimeWorkout = 0,

        /// <summary>
        /// Одна тренировка с трениром.
        /// </summary>
        [InformationAboutWorkout("Одна тренировка с трениром", 200f, 1)]
        OneTimeWorkoutWithTrainer = 1,

        /// <summary>
        /// Четыре тренировки.
        /// </summary>
        [InformationAboutWorkout("4 тренировки", 240f, 4)]
        FourWorkouts = 2,

        /// <summary>
        /// Четыре тренировки с трениром.
        /// </summary>
        [InformationAboutWorkout("4 тренировки с трениром", 700f, 4)]
        FourWorkoutsWithTrainer = 3,

        /// <summary>
        /// Шесть тренировок.
        /// </summary>
        [InformationAboutWorkout("6 тренировок", 365f, 6)]
        SixWorkouts = 4,

        /// <summary>
        /// Шесть тренировок с трениром.
        /// </summary>
        [InformationAboutWorkout("6 тренировок с трениром", 1045f, 6)]
        SixWorkoutsWithTrainer = 5,

        /// <summary>
        /// Восемь тренировок.
        /// </summary>
        [InformationAboutWorkout("8 тренировок", 487f, 8)]
        EightWorkouts = 6,

        /// <summary>
        /// Восемь тренировок с трениром.
        /// </summary>
        [InformationAboutWorkout("8 тренировок с трениром", 1400f, 8)]
        EightWorkoutsWithTrainer = 7,

        /// <summary>
        /// Двенадцать тренировок.
        /// </summary>
        [InformationAboutWorkout("12 тренировок", 730f, 12)]
        TwelveWorkouts = 8,

        /// <summary>
        /// Двенадцать тренировок с трениром.
        /// </summary>
        [InformationAboutWorkout("12 тренировок с трениром", 2090f, 12)]
        TwelveWorkoutsWithTrainer = 9,

        /// <summary>
        /// Тренировка без лимита.
        /// </summary>
        [InformationAboutWorkout("Без лимита", 920f, -1)]
        NoLimits = 10,

        /// <summary>
        /// Тренировка с трениром без лимита.
        /// </summary>
        [InformationAboutWorkout("Без лимита с трениром", 2620f, -1)]
        NoLimitsWithTrainer = 11,
    }
}
