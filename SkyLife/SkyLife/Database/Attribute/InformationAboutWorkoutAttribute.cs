﻿namespace Database.Attribute
{
    using System;

    /// <summary>
    /// Класс для хранения информации про поля в enum NumberWorkouts.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = true)]
    public class InformationAboutWorkoutAttribute : System.Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InformationAboutWorkoutAttribute"/> class.
        /// </summary>
        /// <param name="info">Строка информации.</param>
        /// <param name="price">Цена.</param>
        /// <param name="countWorkout">Количество тренировок.</param>
        public InformationAboutWorkoutAttribute(string info, float price, sbyte countWorkout)
        {
            Price = price;
            Info = info;
            CountWorkout = countWorkout;
        }

        /// <summary>
        /// Gets or sets хранит информацию про информацию.
        /// </summary>
        public string Info { get; set; }

        /// <summary>
        /// Gets or sets харнит информацию про ценну на абонемент.
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// Gets or sets хранит количество тренировок.
        /// </summary>
        public sbyte CountWorkout { get; set; }
    }
}