﻿using System;
using System.Runtime.Serialization;

namespace Database.Exception
{

    [Serializable]
    public class NoMatchingValuesException : System.Exception
    {
        public NoMatchingValuesException() { }
        public NoMatchingValuesException(string message) : base(message) { }
        public NoMatchingValuesException(string message, System.Exception inner) : base(message, inner) { }
        protected NoMatchingValuesException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
