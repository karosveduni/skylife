﻿namespace SkyLife.Popup
{
	using Xamarin.Forms.Xaml;

	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupRating : Rg.Plugins.Popup.Pages.PopupPage
	{
		public PopupRating()
		{
			InitializeComponent();
		}
	}
}