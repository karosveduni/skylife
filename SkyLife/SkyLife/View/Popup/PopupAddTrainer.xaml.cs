﻿namespace SkyLife.Popup
{
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopupAddTrainer : Rg.Plugins.Popup.Pages.PopupPage
    {
        public PopupAddTrainer()
        {
            InitializeComponent();
        }
    }
}