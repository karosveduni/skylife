﻿namespace SkyLife.View
{
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StripePage : ContentPage
    {
        public StripePage()
        {
            InitializeComponent();
        }
    }
}