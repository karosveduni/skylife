﻿namespace SkyLife.View
{
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QrCodeScannerPage : ContentPage
    {
        public QrCodeScannerPage()
        {
            InitializeComponent();
        }
    }
}