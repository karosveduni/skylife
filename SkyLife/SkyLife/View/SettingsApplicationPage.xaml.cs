﻿namespace SkyLife.View
{
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsApplicationPage : ContentView
    {
        public SettingsApplicationPage()
        {
            InitializeComponent();
        }

        private async void Button_Clicked(object sender, System.EventArgs e)
        {
            await Application.Current.MainPage.Navigation.PushAsync(new SettingsProgramPage());
        }

        private async void Button_Clicked_1(object sender, System.EventArgs e)
        {
            await Application.Current.MainPage.Navigation.PushAsync(new CompanyProfilePage());
        }

        private async void Button_Clicked_2(object sender, System.EventArgs e)
        {
            await Application.Current.MainPage.Navigation.PushAsync(new AboutProgramPage());
        }
    }
}