﻿using Xamarin.Forms;

namespace SkyLife.Effect
{
    public class ShowHidePassEffect : RoutingEffect
    {
        public ShowHidePassEffect() : base("Xamarin.ShowHidePassEffect")
        {
        }
    }
}
