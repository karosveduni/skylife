﻿using System;
using System.Globalization;

namespace SkyLife.Attribute
{
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = true)]
    public class CultureInfoLanguageAttribute : System.Attribute
    {
        public CultureInfoLanguageAttribute(string cultureInfo)
        {
            CultureInfo = CultureInfo.GetCultureInfo(cultureInfo);
        }

        private CultureInfo cultureInfo;
        public CultureInfo CultureInfo
        {
            get { return cultureInfo; }
            set { cultureInfo = value; }
        }

    }
}
