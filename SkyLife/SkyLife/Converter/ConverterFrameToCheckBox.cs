﻿using Xamarin.CommunityToolkit.Converters;
using Xamarin.Forms;

namespace SkyLife.Converter
{
    /// <summary>
    /// Класс для проверки цвета.
    /// </summary>
    public class ConverterFrameToCheckBox : BaseConverterOneWay<Color, bool>
    {
        /// <summary>
        /// Gets цвет для сранения.
        /// </summary>
        public static Color DefaultColor { get; } = (Color)Application.Current.Resources["BackgroundColorButton"];

        /// <summary>
        /// Метод к тором происходи ковертация.
        /// </summary>
        /// <param name="value">Цвет для сранения.</param>
        /// <returns>Результат сравнения : true - не равны; false - равны.</returns>
        public override bool ConvertFrom(Color value)
        {
            return value is Color color && color != DefaultColor;
        }
    }
}
