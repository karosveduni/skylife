﻿using System.Collections.Generic;
using System.Linq;

using Database.Enum;
using Database.Helpers;

using Xamarin.CommunityToolkit.Converters;

namespace SkyLife.Converter
{
	internal class ListConverterNumberWorkouts : BaseConverterOneWay<bool, List<string>>
    {
        private static List<string> InformationsWithTrainer { get; set; } = new List<string>();

        private static List<string> Informations { get; set; } = new List<string>();        

		public override List<string> ConvertFrom(bool value)
		{
			if (Informations.Count != 0)
			{
				return value ? InformationsWithTrainer : Informations;
			}

			NumberWorkouts[] arrayWithTrainer = new NumberWorkouts[] { NumberWorkouts.OneTimeWorkoutWithTrainer, NumberWorkouts.FourWorkoutsWithTrainer, NumberWorkouts.SixWorkoutsWithTrainer, NumberWorkouts.EightWorkoutsWithTrainer, NumberWorkouts.TwelveWorkoutsWithTrainer, NumberWorkouts.NoLimitsWithTrainer };

			InformationsWithTrainer.AddRange(arrayWithTrainer.Select(item => NumberWorkoutsAttributeHelper.Current.ConvertItemToAttribute(item).Info));
			
			NumberWorkouts[] array = new NumberWorkouts[] { NumberWorkouts.OneTimeWorkout, NumberWorkouts.FourWorkouts, NumberWorkouts.SixWorkouts, NumberWorkouts.EightWorkouts, NumberWorkouts.TwelveWorkouts, NumberWorkouts.NoLimits };

			Informations.AddRange(array.Select(item => NumberWorkoutsAttributeHelper.Current.ConvertItemToAttribute(item).Info));

			return value ? InformationsWithTrainer : Informations;
		}
	}
}
