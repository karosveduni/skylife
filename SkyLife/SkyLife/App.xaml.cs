﻿namespace SkyLife
{
	using System.ComponentModel;

	using FFImageLoading.Transformations;

	using Prism;
	using Prism.Ioc;

	using SkyLife.Enum;
	using SkyLife.Helpers;
	using SkyLife.Interfaces;
	using SkyLife.Model;
	using SkyLife.Resources.ConstantValues;
	using SkyLife.Resources.Resx;
	using SkyLife.View;
	using SkyLife.ViewModel;

	using Xamarin.CommunityToolkit.Helpers;
	using Xamarin.Essentials;
	using Xamarin.Forms;

	public partial class App
	{
		public App(IPlatformInitializer initializer)
			: base(initializer)
		{
		}

		protected override void RegisterTypes(IContainerRegistry containerRegistry)
		{
			containerRegistry.RegisterSingleton<Storage>().RegisterInstance(Storage.Init());

			containerRegistry.RegisterForNavigation<NavigationPage>();
			containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
			containerRegistry.RegisterForNavigation<CommentPage, CommentPageViewModel>();
			containerRegistry.RegisterForNavigation<AboutProgramPage, AboutProgramPageViewModel>();
			containerRegistry.RegisterForNavigation<ClientPage, ClientPageViewModel>();
			containerRegistry.RegisterForNavigation<CompanyProfilePage, CompanyProfilePageViewModel>();
			containerRegistry.RegisterForNavigation<EditAvatarPage, EditAvatarPageViewModel>();
			containerRegistry.RegisterForNavigation<ManagerPage, ManagerPageViewModel>();
			containerRegistry.RegisterForNavigation<SettingsProgramPage, SettingsProgramPageViewModel>();
			containerRegistry.RegisterForNavigation<StripePage, StripePageViewModel>();
			containerRegistry.RegisterForNavigation<TrainerPage, TrainerPageViewModel>();
			containerRegistry.RegisterForNavigation<WorkoutSettingsPage, WorkoutSettingsPageViewModel>();

			containerRegistry.RegisterSingleton<CollectionStatisticsHelper>();
			containerRegistry.RegisterSingleton<EnumLanguageHelper>();
			containerRegistry.RegisterSingleton<PasswordHelper>();
			containerRegistry.RegisterSingleton<PermissionsHepler>();
			containerRegistry.RegisterSingleton<StripePayHelper>();
			containerRegistry.RegisterSingleton<ToastHelper>();
		}

		protected override async void OnInitialized()
		{
			InitializeComponent();

			Sharpnado.CollectionView.Initializer.Initialize(true, false);

			System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo("en-GB");
			int value = Preferences.Get("LS", int.MinValue);
			if (value != int.MinValue)
			{
				cultureInfo = Container.Resolve<EnumLanguageHelper>().GetCultureInfoFromEnum(value).CultureInfo;
			}

			LocalizationResourceManager.Current.PropertyChanged += LanguageInitialization;
			LocalizationResourceManager.Current.Init(AppResources.ResourceManager, cultureInfo);

			Container.Resolve<CollectionStatisticsHelper>().Statistics();
			Storage storage = Container.Resolve<Storage>();

			DependencyService.Get<IStatusBarPlatformSpecific>().SetStatusBarColor(Color.White, true);

			string email = await SecureStorage.GetAsync("UserEmail");
			string password = await SecureStorage.GetAsync("UserPassword");

			await NavigationService.NavigateAsync($"{ConstantString.NavigationPage}/{ConstantString.LoginPage}");
			//if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password) && storage.SignIn(email, password))
			//{
			//	await NavigationService.NavigateAsync($"{ConstantString.NavigationPage}/{SignInHelper.GetStringPage(storage.CurrentUser.Role)}");
			//}
			//else await NavigationService.NavigateAsync($"{ConstantString.NavigationPage}/{ConstantString.LoginPage}");
		}

		private void LanguageInitialization(object sender, PropertyChangedEventArgs e)
		{
			AppResources.Culture = LocalizationResourceManager.Current.CurrentCulture;
			Language value = Container.Resolve<EnumLanguageHelper>().GetLanguageFromCultureInfo(LocalizationResourceManager.Current.CurrentCulture);
			Preferences.Set("LS", (int)value);
		}
	}
}
