﻿namespace SkyLife.Enum
{
    using SkyLife.Attribute;

    /// <summary>
    /// Список языков на который переведена программа.
    /// </summary>
    public enum Language : byte
    {
        /// <summary>
        /// Англиский язык.
        /// </summary>
        [CultureInfoLanguage("en-GB")]
        English = 0,

        /// <summary>
        /// Русский язык.
        /// </summary>
        [CultureInfoLanguage("ru-RU")]
        Russian = 1,

        /// <summary>
        /// Украинский язык.
        /// </summary>
        [CultureInfoLanguage("uk-UA")]
        Ukrainian = 2,
    }
}
