﻿namespace SkyLife.ModelDTO
{
	using System;

	/// <summary>
	/// Класс для передечи данных.
	/// </summary>
	public class CommentDto
	{
		/// <summary>
		/// Gets or sets хранит id клиента.
		/// </summary>
		public Guid ClientId { get; set; }
		/// <summary>
		/// Gets or sets хранит id тренера.
		/// </summary>
		public Guid TrainerId { get; set; }
	}
}
