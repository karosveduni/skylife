﻿namespace SkyLife.ModelDTO
{
	using System;

	using Database.Model;

	public class TrainerDTO
	{
		public Guid Id { get; set; }
		public byte[] Image { get; set; }
		public string Name { get; set; }
		public float Grade { get; set; }
		public bool IsCheck { get; set; }
		public bool IsVisibleSeparator { get; set; }

		public static TrainerDTO ConvertFromTrainer(Trainer trainer)
		{
			return new TrainerDTO
			{
				Id = trainer.Id,
				Image = trainer.Image,
				Name = trainer.Name,
				Grade = trainer.Grade,
				IsVisibleSeparator = true,
			};
		}
	}
}
