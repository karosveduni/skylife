﻿using Xamarin.Forms;

namespace SkyLife.Resources.Images
{
	public static class Images
	{
		public static ImageSource User { get; } = ImageSource.FromResource("SkyLife.Resources.Images.user.png");
	}
}
