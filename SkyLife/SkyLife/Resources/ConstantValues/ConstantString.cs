﻿namespace SkyLife.Resources.ConstantValues
{
    /// <summary>
    /// Класс для хранения константных строк.
    /// </summary>
    public static class ConstantString
    {
        /// <summary>
        /// Ссылка на мой gitlab.
        /// </summary>
        public const string GitLabUrl = "https://gitlab.com/karosveduni";

        /// <summary>
        /// Хранит номер телефона заведения.
        /// </summary>
        public const string PhoneNumber = "+380 98 765 4321";

		#region Secure Storage
        public const string UserEmail = nameof(UserEmail);
        public const string UserPassword = nameof(UserPassword);
        #endregion

        #region Name Page
        public const string AboutProgramPage = nameof(AboutProgramPage);

        public const string ClientPage = nameof(ClientPage);

        public const string CommentPage = nameof(CommentPage);

        public const string CompanyProfilePage = nameof(CompanyProfilePage);

        public const string EditAvatarPage = nameof(EditAvatarPage);

		public const string LoginPage = nameof(LoginPage);

        public const string ManagerPage = nameof(ManagerPage);

        public const string QrCodeScannerPage = nameof(QrCodeScannerPage);

        public const string SettingsApplicationPage = nameof(SettingsApplicationPage);

        public const string SettingsProfilePage = nameof(SettingsProfilePage);

        public const string SettingsProgramPage = nameof(SettingsProgramPage);

        public const string StripePage = nameof(StripePage);

        public const string TrainerPage = nameof(TrainerPage);

        public const string WorkoutSettingsPage = nameof(WorkoutSettingsPage);

        public const string NavigationPage = nameof(NavigationPage);
        #endregion
        #region Popup
        public const string PopupAddTrainer = nameof(PopupAddTrainer);

        public const string PopupRating = nameof(PopupRating);
		#endregion
	}
}
