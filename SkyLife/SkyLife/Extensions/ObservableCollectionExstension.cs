﻿namespace SkyLife.Extensions
{
	using System.Collections.Generic;
	using System.Collections.ObjectModel;

	public static class ObservableCollectionExstension
	{
		public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> items) => new ObservableCollection<T>(items);
	}
}
