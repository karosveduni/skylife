﻿namespace SkyLife.Model
{
    public class RatingModel
    {
        public byte Rating { get; }
        public string Comment { get; }

        public RatingModel(byte rating, string comment)
        {
            Rating = rating;
            Comment = comment;
        }
    }
}
