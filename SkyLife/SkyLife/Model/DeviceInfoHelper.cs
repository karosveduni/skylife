﻿using SkyLife.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SkyLife.Model
{
    public class DeviceInfoHelper
    {
        public string Name { get; set; } = DeviceInfo.Name;
        public string Model { get; set; } = DeviceInfo.Model;
        public string Manufacturer { get; set; } = DeviceInfo.Manufacturer;
        public string VersionString { get; set; } = DeviceInfo.VersionString;
        public string DevicePlatform { get; set; } = DeviceInfo.Platform.ToString();
        public string DeviceIdiom { get; set; } = DeviceInfo.Idiom.ToString();
        public string Seria { get; set; } = DependencyService.Get<IDevice>().GetIdentifier();

        public static IEnumerable<string> GetPropertys()
        {
            return new string[] { nameof(Name), nameof(Model), nameof(Manufacturer), nameof(VersionString), nameof(DevicePlatform), nameof(DeviceIdiom) };
        }
    }
}
