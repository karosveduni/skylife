﻿namespace SkyLife.Model
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Net.Http;
	using System.Threading.Tasks;

	using Database.Enum;
	using Database.Interfaces;
	using Database.Model;

	using Xamarin.CommunityToolkit.Converters;
	using Xamarin.Essentials;
	using Xamarin.Forms;

	/// <summary>
	/// Класс для хранения информации. Играет роль бекхенда.
	/// </summary>
	public class Storage
	{
		/// </<inheritdoc/>
		private static readonly ByteArrayToImageSourceConverter ImageSourceConverter = new ByteArrayToImageSourceConverter();
		/// <summary>
		/// Initializes a new instance of the <see cref="Storage"/> class.
		/// </summary>
		public Storage()
		{
			Admins = new List<Admin>();
			Clients = new List<Client>();
			Comments = new List<Comment>();
			Managers = new List<Manager>();
			Subscriptions = new List<Subscription>();
			Trainers = new List<Trainer>();
		}

		/// <summary>
		/// Gets хранит список коментариев которые есть в БД.
		/// </summary>
		public List<Comment> Comments { get; private set; }

		/// <summary>
		/// Gets хранит список всех абонентов.
		/// </summary>
		public List<Subscription> Subscriptions { get; private set; }

		/// <summary>
		/// Gets хранит всех клиентов в БД.
		/// </summary>
		public List<Client> Clients { get; private set; }

		/// <summary>
		/// Gets хранит всех трениров в БД.
		/// </summary>
		public List<Trainer> Trainers { get; private set; }

		/// <summary>
		/// Gets хранит всех админов БД.
		/// </summary>
		public List<Admin> Admins { get; private set; }

		/// <summary>
		/// Gets хранит всех менеджеров в БД.
		/// </summary>
		public List<Manager> Managers { get; private set; }

		/// <summary>
		/// Gets хранит список всех ид платежей.
		/// </summary>
		public List<string> IdStripePayments { get; private set; }

		/// <summary>
		/// Хранит статистику про устройства.
		/// </summary>
		public List<DeviceInfoHelper> Statistics { get; private set; }

		/// <summary>
		/// Хранит список рейтинга
		/// </summary>
		public List<Rating> Ratings { get; private set; }

		/// <summary>
		/// Пользователь который вошел в приложение.
		/// </summary>
		public IUser CurrentUser { get; set; }

		public static async Task<Storage> Init()
		{
			Storage storage = new Storage();

			storage.Managers = new List<Manager>
			{
				new Manager
				{
					Email = "emailManager@gmail.com",
					Password = "MasswordManager1",
					Name = "Ирина",
					Image = await ConvertUrlToByteArray(new Uri("https://cs5.livemaster.ru/storage/3d/97/3b5f9c629ca723b57132079fc1lu--svadebnyj-salon-plate-dlya-iry-reki.jpg")),
				},
			};

			storage.Admins = new List<Admin>
			{
				new Admin
				{
					Email = "emailAdmin@gmail.com",
					Password = "PasswordAdmin1",
					Name = "Admin",
					Image = await ConvertUrlToByteArray(new Uri("https://thumbs.dreamstime.com/b/admin-sign-laptop-icon-stock-vector-166205404.jpg")),
				},
			};

			storage.Trainers = new List<Trainer>
			{
				new Trainer(
					await ConvertUrlToByteArray(new Uri("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS49-1FqeLBbg0scP1ixZnJKIahv8AOGixElQ&usqp=CAU")),
					"Олег"
					)
				{
					Email = "emailTrainer@gmail.com",
					Password = "PasswordTrainer1",
				},

				new Trainer(
					await ConvertUrlToByteArray(new Uri("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS49-1FqeLBbg0scP1ixZnJKIahv8AOGixElQ&usqp=CAU")),
					"Илья"
					)
				{
					Email = "emailTrainer1@gmail.com",
					Password = "PasswordTrainer2",
				},
				new Trainer(
					await ConvertUrlToByteArray(new Uri("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS49-1FqeLBbg0scP1ixZnJKIahv8AOGixElQ&usqp=CAU")),
					"Андрей"
					)
				{
					Email = "emailTrainer2@gmail.com",
					Password = "PasswordTrainer2",
				},
				new Trainer(
					await ConvertUrlToByteArray(new Uri("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS49-1FqeLBbg0scP1ixZnJKIahv8AOGixElQ&usqp=CAU")),
					"Саша"
					)
				{
					Email = "emailTraine3@gmail.com",
					Password = "PasswordTrainer3",
				},
			};

			storage.IdStripePayments = new List<string>
			{
				"a10c502d9a86d621e8f598a8be6150f8",
				"8262ef9802b97a772888f8e4bbf8bfd2",
				"58932905760871a9d97c7743dc345242",
				"fb87baa17db09881beae808da7164b95",
				"0da674b914728a0e131a229ba1542868",
			};

			storage.Subscriptions = new List<Subscription>
			{
				new Subscription(NumberWorkouts.SixWorkouts, DateTime.Now, DateTime.Now.AddDays(30), true, storage.IdStripePayments[0]),
			};

			storage.Clients = new List<Client>
			{
				new Client
				{
					Name = "Антон",
					Image = await ConvertUrlToByteArray(new Uri("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT17zMPpTw1kuQHXwqIq4GKYe1yCZMtuUhkPw&usqp=CAU")),
					Email = "email@gmail.com",
					Password = "Password1",
				},
				new Client(storage.Subscriptions[0])
				{
					Name = "Сергей",
					Image = await ConvertUrlToByteArray(new Uri("https://static.wikia.nocookie.net/marvel_dc/images/5/5e/Sergei_Alexandrov_Prime_Earth_0001.JPG/revision/latest?cb=20161127053538")),
					Email = "email2@gmail.com",
					Password = "Password2"
				}
			};

			storage.Comments = new List<Comment>
			{
			};

			storage.Statistics = new List<DeviceInfoHelper>();

			storage.Ratings = new List<Rating>();

			return storage;
		}

		public bool SignIn(string email, string password)
		{
			CurrentUser = FindUser(Clients, email, password);
			if (CurrentUser is null)
			{
				CurrentUser = FindUser(Trainers, email, password);
				if (CurrentUser is null)
				{
					CurrentUser = FindUser(Managers, email, password);
					if (CurrentUser is null)
					{
						CurrentUser = FindUser(Admins, email, password);
						return !(CurrentUser is null);
					}
					else
					{
						return true;
					}
				}
				else
				{
					return true;
				}
			}
			else
			{
				return true;
			}
		}

		private bool FiltersSignIn(IUser user, string email, string password)
			=> user.Email.Equals(email) && user.Password.Equals(password);

		private IUser FindUser<T>(List<T> users, string email, string password)
			where T : IUser
			=> users.Find(item => FiltersSignIn(item, email, password));

		public void SignOut()
		{

		}

		public bool DeleteUser(IUser user)
		{
			return user.Role switch
			{
				UserRole.Client => Clients.RemoveAll(item => item.Id == user.Id) != 0,
				UserRole.Trainer => Trainers.RemoveAll(item => item.Id == user.Id) != 0,
				UserRole.Manager => Managers.RemoveAll(item => item.Id == user.Id) != 0,
				UserRole.Admin => Admins.RemoveAll(item => item.Id == user.Id) != 0,
				_ => throw new ArgumentOutOfRangeException(nameof(user)),
			};
		}

		private static async Task<byte[]> ConvertUrlToByteArray(Uri link)
		{
			using HttpClient http = new HttpClient();
			using HttpResponseMessage message = await http.GetAsync(link);

			return await message.Content.ReadAsByteArrayAsync().ConfigureAwait(false);
			//return ImageSourceConverter.ConvertBackTo(new UriImageSource
			//{
			//	Uri = link,
			//});
		}
	}
}
