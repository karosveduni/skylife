﻿namespace SkyLife.Interfaces
{
    public interface ICloseApplication
    {
        void CloseApplication();
    }
}
