﻿namespace SkyLife.Interfaces
{
    public interface IDevice
    {
        string GetIdentifier();
    }
}
