﻿using Xamarin.Forms;

namespace SkyLife.Interfaces
{
	public interface IStatusBarPlatformSpecific
	{
		void SetStatusBarColor(Color color, bool darkStatusBarTint = false);
	}
}
