﻿namespace SkyLife.ViewModel
{
    using System.Threading.Tasks;
    using System.Windows.Input;

    using Prism.Navigation;
    using Prism.Services;

    using SkyLife.Resources.ConstantValues;
    using SkyLife.Resources.Resx;

    using Xamarin.CommunityToolkit.ObjectModel;
    using Xamarin.Essentials;
    using Xamarin.Forms;

    public class CompanyProfilePageViewModel : BaseViewModel
    {
        public CompanyProfilePageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
        }

        public IAsyncCommand OpenMapsCommand { get; set; }

        public ICommand OpenPhoneDialerCommand { get; set; }

		protected override void InitCommand()
		{
			base.InitCommand();
            OpenMapsCommand = new AsyncCommand(OpenMaps, allowsMultipleExecutions: false);
            OpenPhoneDialerCommand = new Command(OpenPhoneDialer);
		}

		private Task OpenMaps()
        {
            Placemark placemark = new Placemark()
            {
                Locality = AppResources.Address,
            };
            return Map.OpenAsync(placemark);
        }

        private void OpenPhoneDialer()
        {
            PhoneDialer.Open(ConstantString.PhoneNumber);
        }
    }
}