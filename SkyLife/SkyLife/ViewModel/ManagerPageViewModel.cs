﻿namespace SkyLife.ViewModel
{
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Linq;
	using System.Threading.Tasks;

	using Microcharts;

	using Prism.Navigation;
	using Prism.Services;

	using SkiaSharp;

	using SkyLife.Helpers;
	using SkyLife.Model;
	using SkyLife.Popup;
	using SkyLife.Resources.ConstantValues;
	using SkyLife.Resources.Resx;
	using SkyLife.View;

	using Xamarin.CommunityToolkit.ObjectModel;
	using Xamarin.CommunityToolkit.UI.Views;
	using Xamarin.Essentials;
	using Xamarin.Forms;

	public class ManagerPageViewModel : BaseViewModel
	{
		private bool isEnabledStatistics;

		private List<DeviceInfoHelper> deviceInfos;

		private Chart chart;

		private LayoutState layoutState;

		private string currentLayoutState;

		private ObservableCollection<string> propertys;

		private string selectedProperty;

		private Storage storage;

		private CollectionStatisticsHelper collectionStatisticsHelper;

		private ToastHelper toastHelper;

		public ManagerPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, Storage storage, CollectionStatisticsHelper collectionStatisticsHelper, ToastHelper toastHelper)
			: base(navigationService, pageDialogService)
		{
			this.storage = storage;
			this.collectionStatisticsHelper = collectionStatisticsHelper;
			this.toastHelper = toastHelper;
		}

		public bool IsEnabledStatistics
		{
			get { return isEnabledStatistics; }
			set { SetProperty(ref isEnabledStatistics, value); }
		}

		public Chart Chart
		{
			get { return chart; }
			set { SetProperty(ref chart, value); }
		}

		public LayoutState LayoutState
		{
			get { return layoutState; }
			set { SetProperty(ref layoutState, value); }
		}

		public string CurrentLayoutState
		{
			get { return currentLayoutState; }
			set { SetProperty(ref currentLayoutState, value); }
		}

		public ObservableCollection<string> Propertys
		{
			get { return propertys; }
			set { SetProperty(ref propertys, value); }
		}

		public string SelectedProperty
		{
			get { return selectedProperty; }
			set { SetProperty(ref selectedProperty, value, OnSelectedPropertyChanged); }
		}

		public IAsyncCommand OpenMenuCommand { get; set; }

		protected override void InitCommand()
		{
			base.InitCommand();
			OpenMenuCommand = new AsyncCommand(OpenMenu, allowsMultipleExecutions: false);
		}

		public override async void OnAppearing()
		{
			if (IsOnline)
			{
				LayoutState = LayoutState.Loading;
				IEnumerable<DeviceInfoHelper> deviceInfoHelpers = await collectionStatisticsHelper.GetStatistics();

				if (!(deviceInfoHelpers is null))
				{
					deviceInfos = new List<DeviceInfoHelper>(deviceInfoHelpers);
					IsEnabledStatistics = true;
				}

				Propertys = new ObservableCollection<string>(DeviceInfoHelper.GetPropertys());

				SelectedProperty = nameof(DeviceInfoHelper.Name);
				LayoutState = LayoutState.None;
			}
			else
			{
				LayoutState = LayoutState.Custom;
				CurrentLayoutState = "NoInternet";
			}
		}

		private void OnSelectedPropertyChanged()
		{
			IEnumerable<string> dictionary = null;

			dictionary = selectedProperty switch
			{
				nameof(DeviceInfoHelper.Name) => deviceInfos.Select(info => info.Name),
				nameof(DeviceInfoHelper.Model) => dictionary = deviceInfos.Select(info => info.Model),
				nameof(DeviceInfoHelper.Manufacturer) => dictionary = deviceInfos.Select(info => info.Manufacturer),
				nameof(DeviceInfoHelper.VersionString) => dictionary = deviceInfos.Select(info => info.VersionString),
				nameof(DeviceInfoHelper.DevicePlatform) => dictionary = deviceInfos.Select(info => info.DevicePlatform),
				nameof(DeviceInfoHelper.DeviceIdiom) => dictionary = deviceInfos.Select(info => info.DeviceIdiom),
				_ => null,
			};

			IEnumerable<ChartEntry> items = dictionary.GroupBy(info => info).Select(d => new ChartEntry(d.Count())
			{
				Label = d.Key,
				ValueLabel = d.Count().ToString(),
			});

			Random random = new Random(DateTime.Now.Second);
			foreach (ChartEntry item in items)
			{
				SKColor color = new SKColor((byte)random.Next(1, 256), (byte)random.Next(1, 256), (byte)random.Next(1, 256), 255);
				item.ValueLabelColor = color;
				item.Color = color;
				item.TextColor = color;
			}

			Chart = new DonutChart
			{
				Entries = items,
			};
		}

		private async Task QrCodeMethod()
		{
			//Сделать сканирование у пользователя.
            /*if (!string.IsNullOrWhiteSpace(storage.CurrentUser.QRCode))
            {
                var isPayment = storage.IdStripePayments.Contains();
                if (isPayment)
                {
                    var fieldPath = new Plugin.CloudFirestore.FieldPath("Subscription", "Visits");
                    var visits = document.Get<List<DateTime?>>(fieldPath);
                    if (visits != null)
                    {
                        if (visits.Any(item => item != null))
                        {
                            await DisplayInformationAsync(AppResources.CustomersSubscriptionHasExpired);
                            return;
                        }

                        for (int i = 0; i < visits.Count; i++)
                        {
                            if (visits[i] == null)
                            {
                                visits[i] = DateTime.Now;
                                break;
                            }
                        }

                        await Plugin.CloudFirestore.CrossCloudFirestore.Current.Instance.Collection("Users").Document(qrcode).UpdateAsync(fieldPath, visits);

                    }
                }
                else
                {
                    await DisplayInformation(AppResources.ClientNotPaySubscription);
                }
            }*/
		}

		private Task OpenMenu()
		{
			return DisplayListAsync(
				ActionSheetButton.CreateButton(AppResources.Settings, () => NavigationService.NavigateAsync($"{ConstantString.SettingsProfilePage}")),
				ActionSheetButton.CreateButton(AppResources.OpenScanner, async () =>
				{
					if (await PermissionsHepler.CheckingForPermissionsAsync<Permissions.Camera>())
					{
						await NavigationService.NavigateAsync("QrCodeScannerPage");
					}
					else
					{
						await toastHelper.ShowToastAsync(AppResources.PermissionCamera);
					}
				}), 
				ActionSheetButton.CreateButton(AppResources.AddCoach, () => NavigationService.NavigateAsync($"{ConstantString.PopupAddTrainer}")),
				ActionSheetButton.CreateButton<object>(AppResources.Logout, GoBackCommand.Execute, null));
		}
	}
}