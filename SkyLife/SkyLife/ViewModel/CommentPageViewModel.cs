﻿namespace SkyLife.ViewModel
{
    using System.Windows.Input;

    using Database.Model;

    using Prism.Navigation;
    using Prism.Services;

    using SkyLife.Model;
    using SkyLife.ModelDTO;
    using SkyLife.Resources.Resx;

    using Xamarin.CommunityToolkit.ObjectModel;
    using Xamarin.Forms;

    public class CommentPageViewModel : BaseViewModel
    {
        private readonly CommentDto commentDTO;

        private Storage storage;

        public CommentPageViewModel(CommentDto commentDTO, INavigationService navigation, IPageDialogService pageDialogService, Storage storage)
            : base(navigation, pageDialogService)
        {
            this.commentDTO = commentDTO;
            this.storage = storage;
        }

        public byte Grade { get; set; } = 5;

        public string GradeText => $"{AppResources.Grade} {Grade}";

        public string CommentMessage { get; set; }

        public ICommand SendCommentCommand { get; set; }

        public IAsyncCommand CloseCommand { get; set; }

		protected override void InitCommand()
		{
			base.InitCommand();
            SendCommentCommand = new Command(SendComment);
            CloseCommand = new AsyncCommand(NavigationService.GoBackAsync);
		}

		private void SendComment()
        {
            storage.Comments.Add(new Comment
            {
                ClientId = commentDTO.ClientId,
                TrainerId = commentDTO.TrainerId,
                Grade = Grade, 
                Message = CommentMessage,
            });
        }
    }
}