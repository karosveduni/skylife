﻿namespace SkyLife.ViewModel
{
	using System.Collections.ObjectModel;
	using System.Threading.Tasks;

	using Database.Model;

	using Prism.Navigation;
	using Prism.Services;

	using SkyLife.Extensions;
	using SkyLife.Model;
	using SkyLife.Resources.ConstantValues;
	using SkyLife.Resources.Resx;

	using Xamarin.CommunityToolkit.ObjectModel;
	using Xamarin.CommunityToolkit.UI.Views;

	public class TrainerPageViewModel : BaseViewModel
	{
		private Trainer trainer;

		private LayoutState layoutState;

		private string currentLayoutState;

		private ObservableCollection<Client> clients;

		private Storage storage;

		public TrainerPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, Storage storage)
			: base(navigationService, pageDialogService)
		{
			this.storage = storage;
		}

		public Trainer Trainer
		{
			get { return trainer; }
			set { SetProperty(ref trainer, value); }
		}

		public LayoutState LayoutState
		{
			get { return layoutState; }
			set { SetProperty(ref layoutState, value); }
		}

		public string CurrentLayoutState
		{
			get { return currentLayoutState; }
			set { SetProperty(ref currentLayoutState, value); }
		}

		public ObservableCollection<Client> Clients
		{
			get { return clients; }
			set { SetProperty(ref clients, value); }
		}

		public IAsyncCommand OpenMenuCommand { get; set; }

		protected override void InitCommand()
		{
			base.InitCommand();
			OpenMenuCommand = new AsyncCommand(OpenMenu, allowsMultipleExecutions: false);
		}

		public override void OnAppearing()
		{
			base.OnAppearing();
			if (IsOnline)
			{
				//Изменить
				Trainer = storage.CurrentUser as Trainer;

				Clients = storage.Clients.FindAll(item => item.Trainer.Id == Trainer.Id).ToObservableCollection();
			}
			else
			{
				LayoutState = LayoutState.Custom;
				CurrentLayoutState = "NoInternet";
			}
		}

		private Task OpenMenu()
		{
			return DisplayListAsync(
				ActionSheetButton.CreateButton(AppResources.Settings, async () => await NavigationService.NavigateAsync(ConstantString.SettingsProfilePage)),
				ActionSheetButton.CreateButton(AppResources.Logout, storage.SignOut));
		}
	}
}