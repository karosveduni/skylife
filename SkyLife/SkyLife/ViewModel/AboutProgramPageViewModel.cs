﻿namespace SkyLife.ViewModel
{
    using System;
    using System.Threading.Tasks;

    using Prism.Navigation;
    using Prism.Services;

    using SkyLife.Resources.ConstantValues;

    using Xamarin.CommunityToolkit.ObjectModel;
    using Xamarin.Essentials;

    public class AboutProgramPageViewModel : BaseViewModel
    {
        private bool isVisibleButton = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="AboutProgramPageViewModel"/> class.
        /// </summary>
        public AboutProgramPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
        }

        public bool IsVisibleButton
        {
            get { return isVisibleButton; }
            set { SetProperty(ref isVisibleButton, value); }
        }

        public IAsyncCommand OpenSiteCommand { get; set; }

        public IAsyncCommand FeedBackCommand { get; set; }

        public IAsyncCommand OpenPopupRatingCommand { get; set; } 

        protected override void InitCommand()
        {
            base.InitCommand();
            OpenSiteCommand = new AsyncCommand(OpenSite, allowsMultipleExecutions: false);
            FeedBackCommand = new AsyncCommand(Feedback, allowsMultipleExecutions: false);
            OpenPopupRatingCommand = new AsyncCommand(OpenPopupRating, allowsMultipleExecutions: false);
        }

        private Task OpenPopupRating()
        {
            return NavigationService.NavigateAsync("PopupRating");
        }

        private Task OpenSite()
        {
            return Browser.OpenAsync(ConstantString.GitLabUrl);
        }

        private Task Feedback()
        {
            return Email.ComposeAsync("Feedback", $"\n\n{DateTime.Now}", "mailhost1234ad@gmail.com");
        }
    }
}