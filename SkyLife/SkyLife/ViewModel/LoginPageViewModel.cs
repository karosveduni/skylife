﻿namespace SkyLife.ViewModel
{
	using System;
	using System.Threading.Tasks;

	using Database.Enum;

	using Prism.Navigation;
	using Prism.Services;

	using SkyLife.Helpers;
	using SkyLife.Model;
	using SkyLife.Resources.ConstantValues;
	using SkyLife.Resources.Resx;

	using Xamarin.CommunityToolkit.ObjectModel;
	using Xamarin.Essentials;
	using Xamarin.Forms;

	public class LoginPageViewModel : BaseViewModel
	{
		private string email = string.Empty;

		private string password = string.Empty;

		private Storage storage = null;

		private PasswordHelper passwordHelper;

		private ToastHelper toastHelper;

		private bool _isValidPassword;

		public LoginPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, Storage storage, PasswordHelper passwordHelper, ToastHelper toastHelper)
			: base(navigationService, pageDialogService)
		{
			this.storage = storage;
			this.passwordHelper = passwordHelper;
			this.toastHelper = toastHelper;
		}

		public string Pattern => passwordHelper.Pattern;

		public string Email
		{
			get { return email; }
			set { SetProperty(ref email, value); }
		}

		public string Password
		{
			get { return password; }
			set { SetProperty(ref password, value); }
		}

		public bool IsValidPassword
		{
			get => _isValidPassword;
			set => SetProperty(ref _isValidPassword, value);
		}

		public IAsyncCommand LoginCommand { get; private set; }

		public IAsyncCommand RegistrationCommand { get; private set; }

		protected override void InitCommand()
		{
			base.InitCommand();
			LoginCommand = new AsyncCommand(Login, allowsMultipleExecutions: false);
			RegistrationCommand = new AsyncCommand(Registration, allowsMultipleExecutions: false);
		}

		private Task Login()
		{

			if (!IsOnline)
			{
				return toastHelper.ShowToastAsync(AppResources.NotInternetConnection);
			}

			if (!IsValidPassword)
			{
				return toastHelper.ShowToastAsync(Application.Current.MainPage, AppResources.PasswordWrongFormat);
			}

			IsBusy = true;


			if (storage.SignIn(Email, Password))
			{
				IsBusy = false;
				
				return Task.WhenAll(
					SecureStorage.SetAsync(ConstantString.UserEmail, Email.Trim()),
					SecureStorage.SetAsync(ConstantString.UserPassword, Password),
					NavigationService.NavigateAsync(SignInHelper.GetStringPage(storage.CurrentUser.Role)));
			}

			IsBusy = false;

			return toastHelper.ShowToastAsync(Application.Current.MainPage, AppResources.UserDoesNotExist);
		}

		private Task Registration()
		{
			return IsOnline ?
				NavigationService.NavigateAsync(ConstantString.WorkoutSettingsPage) :
				toastHelper.ShowToastAsync(AppResources.NotInternetConnection);
		}
	}
}