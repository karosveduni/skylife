﻿namespace SkyLife.ViewModel
{
	using System.Threading.Tasks;

	using Prism.Navigation;
	using Prism.Services;

	using SkyLife.Model;
	using SkyLife.Resources.Resx;

	using Xamarin.CommunityToolkit.ObjectModel;

	public class SettingsProfilePageViewModel : BaseViewModel
	{
		private Storage storage;

		public SettingsProfilePageViewModel(INavigationService navigation, IPageDialogService pageDialogService, Storage storage)
			: base(navigation, pageDialogService)
		{
			this.storage = storage;
		}

		public IAsyncCommand AccountDeleteCommand { get; set; }

		public IAsyncCommand EditPasswordCommand { get; set; }

		public IAsyncCommand EditEmailCommand { get;set; }

		public IAsyncCommand EditAvatarCommand { get; set; }

		protected override void InitCommand()
		{
			base.InitCommand();
			AccountDeleteCommand = new AsyncCommand(AccountDelete, allowsMultipleExecutions: false);
			EditPasswordCommand = new AsyncCommand(EditPassword, allowsMultipleExecutions: false);
			EditEmailCommand = new AsyncCommand(EditEmail, allowsMultipleExecutions: false);
			EditAvatarCommand = new AsyncCommand(EditAvatar, allowsMultipleExecutions: false);
		}

		private async Task AccountDelete()
		{
			if (await DisplayYesNoAsync(AppResources.DeleteAccount))
			{
				storage.DeleteUser(storage.CurrentUser);
				Xamarin.Essentials.SecureStorage.RemoveAll();
				await NavigationService.GoBackToRootAsync();
			}
		}

		private Task EditPassword()
		{
			return DisplayInformationAsync(AppResources.EmailToMail);
		}

		private Task EditEmail()
		{
			//Добавить строку перевода для новой почты
			//return DisplayInformationAsync();
			return Task.CompletedTask;
		}

		private Task EditAvatar()
		{
			return NavigationService.NavigateAsync("EditAvatar");
		}
	}
}