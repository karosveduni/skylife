﻿namespace SkyLife.ViewModel
{
	using System;
	using System.Collections.ObjectModel;
	using System.Linq;
	using System.Threading.Tasks;

	using Database.Attribute;
	using Database.Enum;
	using Database.Model;

	using Prism.Navigation;
	using Prism.Services;

	using SkyLife.Extensions;
	using SkyLife.Helpers;
	using SkyLife.Model;
	using SkyLife.ModelDTO;
	using SkyLife.Resources.ConstantValues;
	using SkyLife.Resources.Resx;

	using Xamarin.CommunityToolkit.ObjectModel;
	using Xamarin.Essentials;
	using Xamarin.Forms;

	public class WorkoutSettingsPageViewModel : BaseViewModel
	{
		private string email = string.Empty;

		private string password = string.Empty;

		private ImageSource source;

		private string name = string.Empty;

		private ObservableCollection<TrainerDTO> trainerDtos;

		private Storage storage;

		private PasswordHelper passwordHelper;

		private ToastHelper toastHelper;

		private StripePayHelper stripePayHelper;

		private bool isEmailValid;

		private bool isPasswordValid;

		public WorkoutSettingsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, Storage storage, PasswordHelper passwordHelper, ToastHelper toastHelper, StripePayHelper stripePayHelper)
			: base(navigationService, pageDialogService)
		{
			this.storage = storage;
			this.passwordHelper = passwordHelper;
			this.toastHelper = toastHelper;
			this.stripePayHelper = stripePayHelper;
		}

		public string Pattern => passwordHelper.Pattern;

		public string Email
		{
			get { return email; }
			set { SetProperty(ref email, value); }
		}

		public string Password
		{
			get { return password; }
			set { SetProperty(ref password, value); }
		}

		public ImageSource Source
		{
			get { return source; }
			set { SetProperty(ref source, value); }
		}

		public string Name
		{
			get { return name; }
			set { SetProperty(ref name, value); }
		}

		public ObservableCollection<TrainerDTO> TrainerDtos
		{
			get { return trainerDtos; }
			set { SetProperty(ref trainerDtos, value); }
		}

		public bool IsEmailValid
		{
			get { return isEmailValid; }
			set { SetProperty(ref isEmailValid, value); }
		}

		public bool IsPasswordValid
		{
			get { return isPasswordValid; }
			set { SetProperty(ref isPasswordValid, value); }
		}

		public IAsyncCommand ContinueCommand { get; private set; }

		public IAsyncCommand OpenEditAvatarCommand { get; private set; }

		public IAsyncCommand OpenMenuCommand { get; private set; }

		protected override void InitCommand()
		{
			base.InitCommand();
			ContinueCommand = new AsyncCommand(Continue, allowsMultipleExecutions: false);
			OpenEditAvatarCommand = new AsyncCommand(OpenEditAvatar, allowsMultipleExecutions: false);
		}

		private async Task Continue()
		{
			if (!IsPasswordValid && !IsEmailValid)
			{
				IsBusy = false;
				await toastHelper.ShowToastAsync(Application.Current.MainPage, AppResources.PasswordWrongFormat);
				return;
			}

			if (!IsOnline)
			{
				await toastHelper.ShowToastAsync(AppResources.NotInternetConnection);
				return;
			}

			IsBusy = true;

			if (string.IsNullOrWhiteSpace(Name))
			{
				IsBusy = false;
				await DisplayInformationAsync(AppResources.ShouldYou);
				return;
			}

			if (!TrainerDtos.Any(item => item.IsCheck))
			{
				IsBusy = false;
				await DisplayInformationAsync(AppResources.YouForgotChooseCoach);
				return;
			}

			Guid id = trainerDtos.FirstOrDefault(item => item.IsCheck).Id;
			Trainer trainer = storage.Trainers.Find(item => item.Id == id);

			Client client = new Client(new Subscription(default, DateTime.Now, DateTime.Now.AddMonths(1)))
			{
				Name = Name,
				Email = Email.Trim(),
				Password = Password,
				IsEmailVerification = new Random(DateTime.Now.Second).Next(0, 2) == 1,
				Trainer = trainer,
			};

			await Task.WhenAll(SecureStorage.SetAsync("UserEmail", Email.Trim()), SecureStorage.SetAsync("UserPassword", Password));

			//Переделать
			/*if (Source is UriImageSource uri)
            {
                userProfile.PhotoUrl = uri.Uri;
            }

            if (Source is StreamImageSource stream)
            {
                await Plugin.FirebaseStorage.CrossFirebaseStorage.Current.Instance.RootReference.Child("Users").Child(CrossFirebaseAuth.Current.Instance.CurrentUser.Email).Child("Avatar").Child("avatar").PutStreamAsync(await stream.Stream(default));
                userProfile.PhotoUrl = await Plugin.FirebaseStorage.CrossFirebaseStorage.Current.Instance.RootReference.Child("Users").Child(CrossFirebaseAuth.Current.Instance.CurrentUser.Email).Child("Avatar").Child("avatar").GetDownloadUrlAsync();
            }*/

			storage.Clients.Add(client);

			//NumberWorkouts value = Database.Helpers.NumberWorkoutsAttributeHelper.Current.ConvertAttributeToItem(SelectedNumberWorkout);
			//float price = Database.Helpers.NumberWorkoutsAttributeHelper.Current.ConvertItemToAttribute(value).Price;

			//stripePayHelper.Price = price;
			stripePayHelper.Description = AppResources.PaySubscription;

			IsBusy = false;

			await NavigationService.NavigateAsync(ConstantString.StripePage);
		}

		public override void OnAppearing()
		{
			base.OnAppearing();
			IsBusy = true;

			TrainerDtos = storage.Trainers.ConvertAll(TrainerDTO.ConvertFromTrainer).ToObservableCollection();
			TrainerDtos[0].IsVisibleSeparator = false;

			IsBusy = false;
		}

		private Task OpenEditAvatar()
			=> NavigationService.NavigateAsync(ConstantString.EditAvatarPage);
	}
}