﻿namespace SkyLife.ViewModel
{
	using System.Collections.Generic;
	using System.Net;
	using System.Threading.Tasks;

	using Database.Model;

	using Prism.Navigation;
	using Prism.Services;

	using SkyLife.Model;
	using SkyLife.Resources.Resx;
	using SkyLife.View;

	using Xamarin.CommunityToolkit.Helpers;
	using Xamarin.CommunityToolkit.ObjectModel;
	using Xamarin.CommunityToolkit.UI.Views;
	using Xamarin.Forms;

	public class ClientPageViewModel : BaseViewModel
	{
		private Client client;

		private string dateFormat;

		private string activationEmail;

		private Color activationEmailColor;

		private bool isRefreshing;

		private bool isVisibly;

		private string qrContent;

		private LayoutState layoutState;

		private string currentLayoutState;

		private string trainerInformation;

		private Storage storage;

		public ClientPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, Storage storage)
			: base(navigationService, pageDialogService)
		{
			this.storage = storage;
		}

		public Client Client
		{
			get { return client; }
			set { SetProperty(ref client, value); }
		}

		public string DateFormat
		{
			get { return dateFormat; }
			set { SetProperty(ref dateFormat, value); }
		}

		public string ActivationEmail
		{
			get { return activationEmail; }
			set { SetProperty(ref activationEmail, value); }
		}

		public Color ActivationEmailColor
		{
			get { return activationEmailColor; }
			set { SetProperty(ref activationEmailColor, value); }
		}

		public bool IsRefreshing
		{
			get { return isRefreshing; }
			set { SetProperty(ref isRefreshing, value); }
		}

		public bool IsVisibly
		{
			get { return isVisibly; }
			set { SetProperty(ref isVisibly, value); }
		}

		public string QRContent
		{
			get { return qrContent; }
			set { SetProperty(ref qrContent, value); }
		}

		public LayoutState LayoutState
		{
			get { return layoutState; }
			set { SetProperty(ref layoutState, value); }
		}

		public string CurrentLayoutState
		{
			get { return currentLayoutState; }
			set { SetProperty(ref currentLayoutState, value); }
		}

		public string TrainerInformation
		{
			get { return trainerInformation; }
			set { SetProperty(ref trainerInformation, value); }
		}

		public IAsyncCommand RefreshCommand { get; set; }

		public IAsyncCommand OpenMenuCommand { get; set; }

		public IAsyncCommand OpenPageCommentCommand { get; set; }

		protected override void InitCommand()
		{
			base.InitCommand();
			RefreshCommand = new AsyncCommand(Refresh, allowsMultipleExecutions: false);
			OpenMenuCommand = new AsyncCommand(OpenMenu, allowsMultipleExecutions: false);
			OpenPageCommentCommand = new AsyncCommand(OpenPageComment, allowsMultipleExecutions: false);
		}

		public override async void OnAppearing()
		{
			if (!IsOnline)
			{
				LayoutState = LayoutState.Custom;
				CurrentLayoutState = "NoInternet";
				return;
			}

			LayoutState = LayoutState.Loading;

			await LoadDetailsAsync();

			QRContent = storage.CurrentUser.Id.ToString();
			TrainerInformation = Client is null ? string.Empty : $"{Client.Trainer.Name}\n{AppResources.Grade} {Client.Trainer.Grade}";

			LayoutState = LayoutState.None;
		}

		private Task LoadDetailsAsync()
		{
			Client = storage.CurrentUser as Client;

			DateFormat = $"{AppResources.SubscriptionStart}: {Client.Subscription.Start};\n{AppResources.EndSubscription}: {Client.Subscription.End}";

			if (storage.CurrentUser.IsEmailVerification)
			{
				ActivationEmail = AppResources.YourMailActivated;
				ActivationEmailColor = Color.Green;
			}
			else
			{
				ActivationEmail = AppResources.YourMailNotActivated;
				ActivationEmailColor = Color.Red;
			}

			IsVisibly = !(Client.Trainer is null);
			return Task.CompletedTask;
		}

		private async Task Refresh()
		{
			if (!IsOnline)
			{
				LayoutState = LayoutState.Custom;
				CurrentLayoutState = "NoInternet";
			}
			else
			{
				await LoadDetailsAsync();
				LayoutState = LayoutState.None;
			}
			IsRefreshing = false;
		}

		private async Task OpenMenu()
		{
			string answer = await DisplayListAsync(AppResources.OpenSubscription, AppResources.Settings, AppResources.Logout);

			if (answer == AppResources.Settings)
			{
				await Application.Current.MainPage.Navigation.PushAsync(new SettingsProfilePage());
			}
			else if (answer == AppResources.Logout)
			{
				storage.CurrentUser = null;
				Xamarin.Essentials.SecureStorage.RemoveAll();
				await NavigationService.GoBackToRootAsync();
				
			}
			else if (answer == AppResources.OpenSubscription)
			{
				List<string> visits = Client.Subscription.Visits.FindAll(d => d != null).ConvertAll(d => d.Value.ToString(LocalizationResourceManager.Current.CurrentCulture));
				await DisplayInformationAsync(visits.Count == 0 ? AppResources.YouHaventComePracticeYet : string.Join("\n", visits));
			}
		}

		private Task OpenPageComment()
		{
			return NavigationService.NavigateAsync("CommentPage");
		}
	}
}