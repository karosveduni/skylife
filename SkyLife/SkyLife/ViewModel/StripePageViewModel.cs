﻿namespace SkyLife.ViewModel
{
	using System.Threading.Tasks;

	using Prism.Navigation;
	using Prism.Services;

	using SkyLife.Helpers;
	using SkyLife.Resources.ConstantValues;
	using SkyLife.Resources.Resx;

	using Xamarin.CommunityToolkit.ObjectModel;

	public class StripePageViewModel : BaseViewModel
	{
		private string numberCard;

		private string date;

		private string cvv;

		private string stripeText;

		private ToastHelper toastHelper;

		public StripePageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, ToastHelper toastHelper)
			: base(navigationService, pageDialogService)
		{
			this.toastHelper = toastHelper;
			StripeText = $"0 {AppResources.UAH}";
		}

		public string NumberCard
		{
			get { return numberCard; }
			set { SetProperty(ref numberCard, value); }
		}

		public string Date
		{
			get { return date; }
			set { SetProperty(ref date, value); }
		}

		public string CVV
		{
            get { return cvv; }
            set { SetProperty(ref cvv, value); }
		}

		public string StripeText
		{
            get { return stripeText; }
            set { SetProperty(ref stripeText, value); }
		}

		public IAsyncCommand StripePayCommand { get; set; }

		protected override void InitCommand()
		{
			base.InitCommand();
			StripePayCommand = new AsyncCommand(StripePay, allowsMultipleExecutions: false);
		}

		private async Task StripePay()
		{
			if (!IsOnline)
			{
				await toastHelper.ShowToastAsync(AppResources.NotInternetConnection);
			}

			//Видимость оплаты
			await Task.Delay(2000);

			await NavigationService.NavigateAsync(ConstantString.ClientPage);

		}
	}
}