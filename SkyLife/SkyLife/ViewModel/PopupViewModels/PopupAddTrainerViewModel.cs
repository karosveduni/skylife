﻿namespace SkyLife.ViewModel
{
    using System.Threading.Tasks;

    using Database.Model;

    using Prism.Navigation;
    using Prism.Services;

    using SkyLife.Helpers;
    using SkyLife.Model;
    using SkyLife.Resources.Resx;

    using Xamarin.CommunityToolkit.ObjectModel;

    public class PopupAddTrainerViewModel : BaseViewModel
    {
        private string email = string.Empty;

        private string password = string.Empty;

        private string name;

        private string information;

        private Storage storage;

        private PasswordHelper passwordHelper;

        public PopupAddTrainerViewModel(INavigationService navigationService, IPageDialogService pageDialogService, Storage storage, PasswordHelper passwordHelper)
            : base(navigationService, pageDialogService)
        {
            this.storage = storage;
            this.passwordHelper = passwordHelper;
        }

        public string Pattern => passwordHelper.Pattern;

        public string Email
        {
            get { return email; }
            set { SetProperty(ref email, value); }
        }

        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }

        public string Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }

        public string Information
        {
            get { return information; }
            set { SetProperty(ref information, value); }
        }

        public IAsyncCommand SaveCommand { get; set; }

        public IAsyncCommand CloseCommand { get; set; }

        protected override void InitCommand()
        {
            base.InitCommand();
            SaveCommand = new AsyncCommand(Save, allowsMultipleExecutions: false);
            CloseCommand = new AsyncCommand(Close, allowsMultipleExecutions: false);
        }

        private Task Save()
        {
            if (!IsOnline)
            {
                Information = AppResources.NotInternetConnection;
                return Task.CompletedTask;
            }

            if (!passwordHelper.IsCheckFormat(Password))
            {
                Information = AppResources.PasswordWrongFormat;
                return Task.CompletedTask;
            }

            Trainer trainer = new Trainer
            {
                Email = Email.Trim(),
                Password = Password,
                Name = Name,
            };

            if (!storage.Trainers.Contains(trainer))
                storage.Trainers.Add(trainer);
            else
                Information = AppResources.DataEntryError;

            return Close();
        }

        private Task Close()
            => NavigationService.GoBackAsync();
    }
}