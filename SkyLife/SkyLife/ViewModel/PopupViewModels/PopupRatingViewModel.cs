﻿namespace SkyLife.ViewModel
{
    using System.Threading.Tasks;
    using System.Windows.Input;

    using Prism.Navigation;
    using Prism.Services;

    using SkyLife.Model;
    using SkyLife.Resources.Resx;

    using Xamarin.CommunityToolkit.ObjectModel;
    using Xamarin.Forms;

    public class PopupRatingViewModel : BaseViewModel
    {
        private byte rating = 5;

        private string comment;

        private Storage storage;

        public PopupRatingViewModel(INavigationService navigationService, IPageDialogService pageDialogService, Storage storage)
            : base(navigationService, pageDialogService)
        {
            this.storage = storage;
        }

        public string RatingString => $"{AppResources.ProgramEvaluation}: {Rating}";

        public byte Rating
        {
            get => rating;
            set => SetProperty(ref rating, value);
        }

        public string Comment
        {
            get => comment;
            set => SetProperty(ref comment, value);
        }

        public ICommand CloseCommand { get; private set; }

        public IAsyncCommand SendCommand { get; private set; }

        protected override void InitCommand()
        {
            base.InitCommand();
            CloseCommand = new Command(Close);
            SendCommand = new AsyncCommand(SendAsync, allowsMultipleExecutions: false);
        }

        private void Close()
        {
            //переделать
            //MessagingCenter.Send(new AboutProgramViewModel(), "IsVisibleButton");
            //MessagingCenter.Send(new Popup.PopupRating(), "Dismiss");
        }

        private Task SendAsync()
        {
            storage.Ratings.Add(new Database.Model.Rating
            {
                Grade = Rating,
                Comment = Comment,
            });
            return NavigationService.GoBackAsync();
        }
    }
}