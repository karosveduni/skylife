﻿namespace SkyLife.ViewModel
{
	using System.Collections.ObjectModel;
	using System.Globalization;
	using System.Windows.Input;

	using Prism.Navigation;
	using Prism.Services;

	using Xamarin.CommunityToolkit.Helpers;
	using Xamarin.Forms;

	public class SettingsProgramPageViewModel : BaseViewModel
	{
		private CultureInfo selectCulture;

		private ObservableCollection<CultureInfo> cultures = new ObservableCollection<CultureInfo>(new CultureInfo[] { CultureInfo.GetCultureInfo("ru-RU"), CultureInfo.GetCultureInfo("uk-UA"), CultureInfo.GetCultureInfo("en-GB") });

		public SettingsProgramPageViewModel(INavigationService navigationService, IPageDialogService pageDialogServer)
			: base(navigationService, pageDialogServer)
		{
			SelectedCulture = LocalizationResourceManager.Current.CurrentCulture;
		}

		public CultureInfo SelectedCulture
		{
			get { return selectCulture; }
			set { SetProperty(ref selectCulture, value); }
		}

		public ObservableCollection<CultureInfo> Cultures
		{
			get { return cultures; }
			set { SetProperty(ref cultures, value); }
		}

		public ICommand SaveCommand { get; set; }

		protected override void InitCommand()
		{
			base.InitCommand();
			SaveCommand = new Command(Save);
		}

		private void Save()
		{
			LocalizationResourceManager.Current.CurrentCulture = SelectedCulture;
		}
	}
}