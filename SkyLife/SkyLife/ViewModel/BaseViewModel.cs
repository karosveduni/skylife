﻿namespace SkyLife.ViewModel
{
    using System.Threading.Tasks;

    using Plugin.Connectivity;

    using Prism.AppModel;
    using Prism.Mvvm;
    using Prism.Navigation;
    using Prism.Services;

    using SkyLife.Resources.Resx;

    using Xamarin.CommunityToolkit.ObjectModel;

    public abstract class BaseViewModel : BindableBase, IInitialize, INavigationAware, IDestructible, IPageLifecycleAware
    {
        private bool isBusy = false;
        private IPageDialogService pageDialogService;

        protected BaseViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
        {
            NavigationService = navigationService;
            this.pageDialogService = pageDialogService;

            InitCommand();
        }

        protected INavigationService NavigationService { get; private set; }

        public bool IsOnline => CrossConnectivity.Current.IsConnected;

        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        public IAsyncCommand GoBackCommand { get; private set; }

        public Task DisplayInformationAsync(string message)
        {
            return pageDialogService.DisplayAlertAsync(AppResources.Information, message, AppResources.Close, FlowDirection.LeftToRight);
        }

        public Task<bool> DisplayYesNoAsync(string message)
        {
            return pageDialogService.DisplayAlertAsync(AppResources.Information, message, AppResources.Yes, AppResources.No, FlowDirection.LeftToRight);
        }

        public Task<string> DisplayListAsync(params string[] message)
        {
            return pageDialogService.DisplayActionSheetAsync(AppResources.Information, AppResources.Close, null, FlowDirection.LeftToRight, message);
        }

        public Task DisplayListAsync(params IActionSheetButton[] actionSheetButtons)
        {
            return pageDialogService.DisplayActionSheetAsync(AppResources.Information, FlowDirection.LeftToRight, actionSheetButtons);
        }

        public virtual void Initialize(INavigationParameters parameters)
        {
        }

        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public virtual void OnNavigatedTo(INavigationParameters parameters)
        {
        }

        public virtual void Destroy()
        {
        }

        public virtual void OnAppearing()
        {
        }

        public virtual void OnDisappearing()
        {
        }

        protected virtual void InitCommand()
        {
            GoBackCommand = new AsyncCommand(NavigationService.GoBackAsync, allowsMultipleExecutions: false);
        }
    }
}
