﻿namespace SkyLife.ViewModel
{
	using System.Diagnostics;
	using System.IO;
	using System.Linq;
	using System.Threading.Tasks;
	using System.Windows.Input;

	using Database.Model;

	using NativeMedia;

	using Prism.Navigation;
	using Prism.Services;

	using SkyLife.Helpers;
	using SkyLife.Model;
	using SkyLife.Resources.Resx;

	using Xamarin.CommunityToolkit.Converters;
	using Xamarin.CommunityToolkit.ObjectModel;
	using Xamarin.Essentials;
	using Xamarin.Forms;

	public class EditAvatarPageViewModel : BaseViewModel
	{
		private ImageSource imageSource;

		private bool isVisible = true;

		private Storage storage;

		private ToastHelper toastHelper;

		private IPageDialogService pageDialogService;

		private ByteArrayToImageSourceConverter _imageSourceConverter;

		public EditAvatarPageViewModel(INavigationService navigation, IPageDialogService pageDialogService, Storage storage, ToastHelper toastHelper)
			: base(navigation, pageDialogService)
		{
			this.pageDialogService = pageDialogService;
			this.toastHelper = toastHelper;
			this.storage = storage;
			_imageSourceConverter = new ByteArrayToImageSourceConverter();
		}

		public ImageSource ImageSource
		{
			get { return imageSource; }
			set { SetProperty(ref imageSource, value); }
		}

		public bool IsVisible
		{
			get { return isVisible; }
			set { SetProperty(ref isVisible, value); }
		}

		public IAsyncCommand ParseLinkCommand { get; set; }

		public IAsyncCommand SelectImageFromGalleryCommand { get; set; }

		public IAsyncCommand SaveAvatarCommand { get; set; }

		public ICommand ClearAvatarCommand { get; set; }

		public ICommand OpenCameraCommand { get; set; }

		public override void Initialize(INavigationParameters parameters)
		{
			if (!(storage.CurrentUser is null))
			{
				ImageSource = _imageSourceConverter.ConvertFrom(storage.CurrentUser.Image);
				IsVisible = true;
			}
			else
			{
				IsVisible = false;
			}
		}

		protected override void InitCommand()
		{
			base.InitCommand();
			ParseLinkCommand = new AsyncCommand(ParseLink, allowsMultipleExecutions: false);
			SelectImageFromGalleryCommand = new AsyncCommand(SelectImageFromGallery, allowsMultipleExecutions: false);
			SaveAvatarCommand = new AsyncCommand(SaveAvatar, allowsMultipleExecutions: false);
			ClearAvatarCommand = new Command(ClearAvatar);
			OpenCameraCommand = new AsyncCommand(OpenCamera, allowsMultipleExecutions: false);
		}

		private async Task ParseLink()
		{
			string result = await pageDialogService.DisplayPromptAsync(AppResources.PasteLinkField, "", "Parser", "Cancel", keyboardType: Prism.AppModel.KeyboardType.Url);

			//Сохранять в byte[]
		}

		private async Task SelectImageFromGallery()
		{
			if (await PermissionsHepler.CheckingForPermissionsAsync<Permissions.StorageWrite>())
			{
				MediaPickRequest photo = new MediaPickRequest(1, MediaFileType.Image)
				{
					PresentationSourceBounds = System.Drawing.Rectangle.Empty,
					Title = "select image",
					UseCreateChooser = true,
				};

				MediaPickResult result = await MediaGallery.PickAsync(photo);
				IMediaFile file = result.Files?.First();

				if (!(file is null))
				{
#if DEBUG
					Debug.WriteLine(file.ContentType);
					Debug.WriteLine(file.Type);
					Debug.WriteLine(file.Extension);
					Debug.WriteLine(file.NameWithoutExtension);
#endif
					Stream stream = await file.OpenReadAsync();
					ImageSource = ImageSource.FromStream(() => stream);
				}
			}
			else
			{
				await toastHelper.ShowToastAsync(AppResources.PermissionGallery);
			}
		}

		private Task SaveAvatar()
		{
			if (!(storage.CurrentUser is null))
			{
				User client = new Client
				{
					Image = _imageSourceConverter.ConvertBackTo(ImageSource)
				};

				storage.CurrentUser = client;
			}
			else
			{
				//MessagingCenter.Send(new WorkoutSettingsViewModel(), "Avatar", (object)ImageSource);
				return GoBackCommand.ExecuteAsync();
			}

			return Task.CompletedTask;
		}

		private void ClearAvatar()
		{
			storage.CurrentUser.ClearImage();
		}

		private async Task OpenCamera()
		{
			if (await PermissionsHepler.CheckingForPermissionsAsync<Permissions.Camera>())
			{
				MediaPickRequest photo = new MediaPickRequest(1, MediaFileType.Image)
				{
					PresentationSourceBounds = System.Drawing.Rectangle.Empty,
					Title = "select image",
					UseCreateChooser = true,
				};

				MediaPickResult result = await MediaGallery.PickAsync(photo);
				IMediaFile file = result.Files?.First();

				if (!(file is null))
				{
#if DEBUG
					Debug.WriteLine(file.ContentType);
					Debug.WriteLine(file.Type);
					Debug.WriteLine(file.Extension);
					Debug.WriteLine(file.NameWithoutExtension);
#endif
					Stream stream = await file.OpenReadAsync();
					ImageSource = ImageSource.FromStream(() => stream);
				}
			}
			else
			{
				await toastHelper.ShowToastAsync(AppResources.PermissionCamera);
			}
		}
	}
}