﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "<Ожидание>", Scope = "member", Target = "~M:Database.Attribute.InformationAboutWorkoutAttribute.#ctor(System.String,System.Single,System.SByte)")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "<Ожидание>", Scope = "namespace", Target = "~N:Database.Attribute")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "<Ожидание>", Scope = "namespace", Target = "~N:Database.Enum")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "<Ожидание>", Scope = "namespace", Target = "~N:Database.Helpers")]
[assembly: SuppressMessage("Major Code Smell", "S1144:Unused private types or members should be removed", Justification = "<Ожидание>", Scope = "member", Target = "~M:Database.Helpers.NumberWorkoutsAttributeHelper.BaseConverter(System.Func{Database.Attribute.InformationAboutWorkoutAttribute,System.Boolean})~Database.Enum.NumberWorkouts")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "<Ожидание>", Scope = "member", Target = "~M:Database.Helpers.NumberWorkoutsAttributeHelper.BaseConverter(System.Func{Database.Attribute.InformationAboutWorkoutAttribute,System.Boolean})~Database.Enum.NumberWorkouts")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "<Ожидание>", Scope = "member", Target = "~M:Database.Helpers.NumberWorkoutsAttributeHelper.ConvertAttributeToItem(Database.Attribute.InformationAboutWorkoutAttribute)~Database.Enum.NumberWorkouts")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "<Ожидание>", Scope = "member", Target = "~M:Database.Helpers.NumberWorkoutsAttributeHelper.ConvertAttributeToItem(System.String)~Database.Enum.NumberWorkouts")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "<Ожидание>", Scope = "namespace", Target = "~N:Database.Interfaces")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "<Ожидание>", Scope = "member", Target = "~M:SkyLife.Helpers.ToastHelper.#ctor")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "<Ожидание>", Scope = "member", Target = "~M:SkyLife.Helpers.ToastHelper.ShowToastAsync(Xamarin.Forms.VisualElement,System.String)~System.Threading.Tasks.Task")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "<Ожидание>", Scope = "member", Target = "~M:SkyLife.Helpers.ToastHelper.ShowToastAsync(System.String)~System.Threading.Tasks.Task")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "<Ожидание>", Scope = "member", Target = "~M:SkyLife.Helpers.ToastHelper.ShowSnackBarAsync(Xamarin.Forms.VisualElement,System.String,System.String,System.Func{System.Threading.Tasks.Task})~System.Threading.Tasks.Task")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "<Ожидание>", Scope = "member", Target = "~M:SkyLife.Helpers.ToastHelper.ShowSnackBarAsync(System.String,System.String,System.Func{System.Threading.Tasks.Task})~System.Threading.Tasks.Task")]
