﻿using Foundation;

using SkyLife.Interfaces;
using SkyLife.iOS.CustomRenderers;

using UIKit;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: Dependency(typeof(StatusBarIOS))]

namespace SkyLife.iOS.CustomRenderers
{
	public class StatusBarIOS : IStatusBarPlatformSpecific
	{
		public void SetStatusBarColor(Color color, bool darkStatusBarTint = false)
		{
			if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
			{
				UIView statusBar = new UIView(UIApplication.SharedApplication.KeyWindow.WindowScene.StatusBarManager.StatusBarFrame);
				statusBar.BackgroundColor = color.ToUIColor();
				UIApplication.SharedApplication.KeyWindow.AddSubview(statusBar);
			}
			else
			{
				UIView statusBar = UIApplication.SharedApplication.ValueForKey(new NSString("statusBar")) as UIView;
				if (statusBar.RespondsToSelector(new ObjCRuntime.Selector("setBackgroundColor:")))
				{
					statusBar.BackgroundColor = color.ToUIColor();
				}
			}

			UIStatusBarStyle style = darkStatusBarTint ? UIStatusBarStyle.DarkContent : UIStatusBarStyle.LightContent;
			UIApplication.SharedApplication.SetStatusBarStyle(style, false);
			Xamarin.Essentials.Platform.GetCurrentUIViewController()?.SetNeedsStatusBarAppearanceUpdate();
		}
	}
}