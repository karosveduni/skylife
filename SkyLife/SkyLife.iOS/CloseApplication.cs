﻿using System.Diagnostics;

using SkyLife.Interfaces;
using SkyLife.iOS;

using Xamarin.Forms;

[assembly: Dependency(typeof(CloseApplication))]

namespace SkyLife.iOS
{
    public class CloseApplication : ICloseApplication
    {
        void ICloseApplication.CloseApplication()
        {
            Process.GetCurrentProcess().Kill();
        }
    }
}