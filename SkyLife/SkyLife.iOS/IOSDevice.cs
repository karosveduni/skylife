﻿using SkyLife.Interfaces;
using SkyLife.iOS;

using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(IOSDevice))]

namespace SkyLife.iOS
{
    public class IOSDevice : IDevice
    {
        public string GetIdentifier()
        {
            return UIDevice.CurrentDevice.IdentifierForVendor.ToString();
        }
    }
}