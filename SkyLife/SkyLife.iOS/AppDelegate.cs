﻿namespace SkyLife.iOS
{
	using System.Linq;

	using Foundation;

	using Sharpnado.CollectionView.iOS;

	using UIKit;

	// The UIApplicationDelegate for the application. This class is responsible for launching the 
	// User Interface of the application, as well as listening (and optionally responding) to 
	// application events from iOS.
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		//
		// This method is invoked when the application has loaded and is ready to run. In this 
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			NativeMedia.Platform.Init(GetTopViewController);
			Initializer.Initialize();
			global::Xamarin.Forms.Forms.Init();
			FFImageLoading.Forms.Platform.CachedImageRenderer.Init();
			FFImageLoading.Forms.Platform.CachedImageRenderer.InitImageSourceHandler();
			LoadApplication(new App(new IOSInitializer()));

			return base.FinishedLaunching(app, options);
		}

		public UIViewController GetTopViewController()
		{
			UIViewController vc = UIApplication.SharedApplication.KeyWindow.RootViewController;

			return vc is UINavigationController navController ?
				navController.ViewControllers.Last() :
				vc;
		}
	}
}
